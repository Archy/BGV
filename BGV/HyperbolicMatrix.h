#pragma once
#include <directxmath.h>

class HypMat
{
private:
	static const DirectX::XMMATRIX I_31;
	static const DirectX::XMFLOAT4X4 I;

public:
	HypMat();
	~HypMat();

public:	//Euclidean 

	static DirectX::XMFLOAT4X4 add(const DirectX::XMFLOAT4X4 &A, const DirectX::XMFLOAT4X4 &b);

	static DirectX::XMFLOAT4X4 sub(const DirectX::XMFLOAT4X4 &A, const DirectX::XMFLOAT4X4 &b);

	static DirectX::XMMATRIX XMMatrixTranslation(float OffsetX, float OffsetY, float OffsetZ, float OffsetW);

	static DirectX::XMMATRIX XMMatrixTranslationFromVector(DirectX::XMVECTOR vector);

	static DirectX::XMVECTOR add(const DirectX::XMVECTOR &a, const DirectX::XMVECTOR &b);

	static DirectX::XMVECTOR sub(const DirectX::XMVECTOR &a, const DirectX::XMVECTOR &b);


public:	//Hyperbolic

	static void toAffine(DirectX::XMFLOAT4& v);

	static void normalize(DirectX::XMVECTOR & a);

	static float innerProduct(const DirectX::XMVECTOR &a, const DirectX::XMVECTOR &b);

	static DirectX::XMMATRIX reflectionMatrix(const DirectX::XMVECTOR &point);

	static DirectX::XMMATRIX translationMatrix(const DirectX::XMVECTOR &a, const DirectX::XMVECTOR &b);

	static DirectX::XMMATRIX rotationMatrix(const DirectX::XMVECTOR & la, const DirectX::XMVECTOR & lb, const double angle);

};

