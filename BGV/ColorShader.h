#pragma once
#include "BaseShader.h"


class ColorShader : 
	public BaseShader
{
public:
	ColorShader();
	~ColorShader();

	//sets the shader parameters and then draws the prepared model vertices.
	//Throws expetion on error
	void Render(CComPtr<ID3D11DeviceContext> deviceContext,
		int indexCount, int indexOffset, int vertexOffset,
		const DirectX::XMMATRIX &worldMatrix, 
		const DirectX::XMMATRIX &viewProjectionMatrix);

private:
	virtual void createLayout(CComPtr<ID3D11Device> device);

	bool SetShaderParameters(CComPtr<ID3D11DeviceContext>, const DirectX::XMMATRIX&,
		const DirectX::XMMATRIX &viewProjectionMatrix);
	
	void RenderShader(CComPtr<ID3D11DeviceContext>, int, int, int);
};

