cbuffer cbPerObject
{
	float4x4 gWorldViewProj;
};
struct VertexIn
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
	//for hardware instancing
	row_major float4x4 World : WORLD;
	float4 ColorTwo : COLORTWO;
	uint InstanceId : SV_InstanceID;
};
struct VertexOut
{
	float4 PosH  : SV_POSITION;
	float4 Color : COLOR;
};
VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	float3 PosW;

	// Przekszta do przestrzeni wiata.
	PosW = mul(float4(vin.PosL, 1.0f), vin.World).xyz;

	// Transform to homogeneous clip space.
	// Przeksztacenie do jednorodnej przestrzeni obcinania
	vout.PosH = mul(float4(PosW, 1.0f), gWorldViewProj);

	// Just pass vertex color into the pixel shader.
	vout.Color = vin.ColorTwo;
	return vout;
}
float4 PS(VertexOut pin) : SV_Target
{
	return pin.Color;
}
technique11 ColorTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}