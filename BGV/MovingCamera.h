#pragma once
#include "Camera.h"
class MovingCamera :
	public Camera
{
public:
	MovingCamera();
	~MovingCamera();

	void rotate(float dTheta, float dPhi);
	void move(float dRadius);

private:
	void updatePosition();

private:
	float mTheta = 1.5f*3.1415f;
	float mPhi = 0.25f*3.1415f;
	float mRadius = 50.0f;

	const float MAX_RADIUS = 500.0f;
	const float MIN_RADIUS = 3.0f;
};

