#include "BaseShader.h"

//CComPtr<ID3DX11Effect> BaseShader::sFX = nullptr;
//CComPtr<ID3DX11EffectTechnique> BaseShader::sTech = nullptr;
//CComPtr<ID3DX11EffectMatrixVariable> BaseShader::sfxWorldViewProj = nullptr;

BaseShader::BaseShader() :
	mLayout(nullptr)
{
}


BaseShader::~BaseShader()
{
}

void BaseShader::Initialize(CComPtr<ID3D11Device> device, const std::string & shaderFile)
{
	InitializeShader(device, shaderFile);

	createLayout(device);
}

void BaseShader::InitializeShader(CComPtr<ID3D11Device> device, const std::string & shaderFile)
{
	HRESULT result;
	DWORD shaderFlags = 0;

	CComPtr<ID3D10Blob> compiledShader = 0;
	CComPtr<ID3D10Blob> compilationMsgs = 0;

	//convert string shader file name to LPCWSTR for
	std::wstring stemp = std::wstring(shaderFile.begin(), shaderFile.end());
	LPCWSTR sw = stemp.c_str();

	result = D3DCompileFromFile(sw, 0,
		0, 0, "fx_5_0", shaderFlags,
		0, &compiledShader, &compilationMsgs);

	// compilationMsgs przechowuje komunikaty o b��dach lub ostrze�enia.
	if (compilationMsgs != 0)
	{
		MessageBoxA(0, (char*)compilationMsgs->GetBufferPointer(), 0, 0);
	}
	// Nawet je�li compilationMsgs jest pusta, upewnij si�, �e
	// nie by�o innych b��d�w.
	if (!SUCCEEDED(result))
	{
		throw GameError(FATAL_ERROR, "Error compiling shader");
	}


	//create effect
	result = D3DX11CreateEffectFromMemory(
		compiledShader->GetBufferPointer(),
		compiledShader->GetBufferSize(),
		0, device, &mFX);

	if (FAILED(result))
	{
		throw GameError(FATAL_ERROR, "Error creating effect");
	}

	mTech = mFX->GetTechniqueByName("ColorTech");
	mfxWorldViewProj = mFX->GetVariableByName("gWorldViewProj")->AsMatrix();

}

