#include "InputHandler.h"



InputHandler::InputHandler() : 
	mMouseX(0), mMouseY(0), mMouseLeft(false), mMouseRight(false), 
	mMouseWheelMoved(false)

{
	for (int i = 0; i < KEYS_ARRAY_LEN; ++i) {
		mKeysDown[i] = false;
		mKeysPressed[i] = false;
	}
}


InputHandler::~InputHandler()
{
}

void InputHandler::Initialize(HWND hwnd)
{
	//TODO capture hi-def mouse
}

void InputHandler::setMousePos(int x, int y)
{
	mMouseX = x;
	mMouseY = y;
}

void InputHandler::mouseWheelMove(WPARAM wParam)
{
	mMouseWheelMoved = true;
	mMouseWheel = GET_WHEEL_DELTA_WPARAM(wParam);
}

void InputHandler::keyDown(WPARAM wParam)
{
	if (wParam < KEYS_ARRAY_LEN)
	{
		mKeysDown[wParam] = true; // Update keysDown array
		mKeysPressed[wParam] = true; // Update keysPressed array
	}
}

void InputHandler::keyUp(WPARAM wParam)
{
	if (wParam < KEYS_ARRAY_LEN)
	{
		mKeysDown[wParam] = false; // Update keysDown array
		mKeysPressed[wParam] = false; // Update keysPressed array
	}
}

bool InputHandler::isKeyDown(UCHAR vkey) const
{
	if (vkey < KEYS_ARRAY_LEN)
		return mKeysPressed[vkey];
	else
		return false;
}

bool InputHandler::wasKeyPressed(UCHAR vkey) const
{
	if (vkey < KEYS_ARRAY_LEN)
		return mKeysPressed[vkey];
	else
		return false;
}

bool InputHandler::anyKeyPressed() const
{
	for (size_t i = 0; i < KEYS_ARRAY_LEN; i++)
		if (mKeysPressed[i] == true)
			return true;
	return false;
}

void InputHandler::clearKeysPress()
{
	mMouseWheelMoved = false;

	for (size_t i = 0; i < KEYS_ARRAY_LEN; i++)
		mKeysPressed[i] = false;
}
