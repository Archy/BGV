#include "HyperbolicMatrix.h"
#include <cmath>
#include <iostream>

using namespace DirectX;

const XMMATRIX HypMat::I_31 = XMMATRIX(1, 0, 0, 0,
										0, 1, 0, 0,
										0, 0, 1, 0,
										0, 0, 0, -1);
const XMFLOAT4X4 HypMat::I = XMFLOAT4X4(1, 0, 0, 0,
										0, 1, 0, 0,
										0, 0, 1, 0,
										0, 0, 0, 1);


HypMat::HypMat()
{
}


HypMat::~HypMat()
{
}

XMFLOAT4X4 HypMat::add(const DirectX::XMFLOAT4X4 & a, const DirectX::XMFLOAT4X4 & b)
{
	return XMFLOAT4X4(
		a._11+b._11, a._12+b._12, a._13+b._13, a._14+b._14,
		a._21+b._21, a._22+b._22, a._23+b._23, a._24+b._24,
		a._31+b._31, a._32+b._32, a._33+b._33, a._34+b._34,
		a._41+b._41, a._42+b._42, a._43+b._43, a._44+b._44
	);
}

XMFLOAT4X4 HypMat::sub(const DirectX::XMFLOAT4X4 & a, const DirectX::XMFLOAT4X4 & b)
{
	return XMFLOAT4X4(
		a._11-b._11, a._12-b._12, a._13-b._13, a._14-b._14,
		a._21-b._21, a._22-b._22, a._23-b._23, a._24-b._24,
		a._31-b._31, a._32-b._32, a._33-b._33, a._34-b._34,
		a._41-b._41, a._42-b._42, a._43-b._43, a._44-b._44
	);
}

DirectX::XMMATRIX HypMat::XMMatrixTranslation(float OffsetX, float OffsetY, float OffsetZ, float OffsetW)
{
	XMFLOAT4X4 tmp;
	XMStoreFloat4x4(&tmp, DirectX::XMMatrixTranslation(OffsetX, OffsetY, OffsetZ));
	tmp.m[3][3] = OffsetW;

	return DirectX::XMLoadFloat4x4(&tmp);
}

DirectX::XMMATRIX HypMat::XMMatrixTranslationFromVector(XMVECTOR vector)
{
	XMFLOAT4X4 tmp;
	XMStoreFloat4x4(&tmp, DirectX::XMMatrixTranslationFromVector(vector));
	tmp.m[3][3] = DirectX::XMVectorGetW(vector);

	return DirectX::XMLoadFloat4x4(&tmp);
}

DirectX::XMVECTOR HypMat::add(const DirectX::XMVECTOR & a, const DirectX::XMVECTOR & b)
{
	XMFLOAT4 tmpA, tmpB;
	DirectX::XMStoreFloat4(&tmpA, a);
	DirectX::XMStoreFloat4(&tmpB, b);

	XMFLOAT4 result(
		tmpA.x + tmpB.x,
		tmpA.y + tmpB.y,
		tmpA.z + tmpB.z,
		tmpA.w + tmpB.w
	);

	return DirectX::XMLoadFloat4(&result);
}

DirectX::XMVECTOR HypMat::sub(const DirectX::XMVECTOR & a, const DirectX::XMVECTOR & b)
{
	XMFLOAT4 tmpA, tmpB;
	DirectX::XMStoreFloat4(&tmpA, b);
	DirectX::XMStoreFloat4(&tmpB, a);

	XMFLOAT4 result(
		tmpA.x - tmpB.x,
		tmpA.y - tmpB.y,
		tmpA.z - tmpB.z,
		tmpA.w - tmpB.w
	);
	return DirectX::XMLoadFloat4(&result);
}


//DirectX::XMMATRIX HypMat::innerProduct(const DirectX::XMMATRIX & A, const DirectX::XMMATRIX & B)
//{
//	return DirectX::XMMatrixMultiply(
//		DirectX::XMMatrixTranspose(A),
//		DirectX::XMMatrixMultiply(I, B)
//	);
//}

float HypMat::innerProduct(const XMVECTOR & a, const XMVECTOR & b)
{
	//Minkowski inner product
	return DirectX::XMVectorGetX(DirectX::XMVector4Dot(
		a,
		DirectX::XMVector4Transform(b, I_31)
	));
}

XMMATRIX HypMat::reflectionMatrix(const XMVECTOR & point)
{
	XMFLOAT4 tempPoint;
	DirectX::XMStoreFloat4(&tempPoint, point);
	const DirectX::XMMATRIX temp1 = XMMATRIX(tempPoint.x, 0, 0, 0,
										0, tempPoint.y, 0, 0,
										0, 0, tempPoint.z, 0,
										0, 0, 0, tempPoint.w);
	const DirectX::XMMATRIX temp2 = XMMATRIX(
		tempPoint.x, tempPoint.y, tempPoint.z, tempPoint.w,
		tempPoint.x, tempPoint.y, tempPoint.z, tempPoint.w,
		tempPoint.x, tempPoint.y, tempPoint.z, tempPoint.w,
		tempPoint.x, tempPoint.y, tempPoint.z, tempPoint.w);

	const auto resutMatrix = ((2.0f * (temp1 * temp2)) * I_31) / innerProduct(point, point);
	XMFLOAT4X4 tempMatrix;
	DirectX::XMStoreFloat4x4(&tempMatrix, resutMatrix);
	const XMFLOAT4X4 result = sub(I, tempMatrix);

	return DirectX::XMLoadFloat4x4(&result);
}

XMMATRIX HypMat::translationMatrix(const DirectX::XMVECTOR & a, const DirectX::XMVECTOR & b)
{
	float abProd = innerProduct(a, b);
	XMVECTOR m = DirectX::XMVectorScale(a, std::sqrt(innerProduct(b, b)*abProd)) + DirectX::XMVectorScale(b, std::sqrt(innerProduct(a, a)*abProd));

	return reflectionMatrix(m) * reflectionMatrix(a);
}

XMMATRIX HypMat::rotationMatrix(const XMVECTOR & la, const XMVECTOR & lb, const double angle)
{
	if (angle == 0.0)
		return DirectX::XMMatrixIdentity();

	XMFLOAT4 temp;
	DirectX::XMStoreFloat4(&temp, la);
	toAffine(temp);
	const XMVECTOR a = DirectX::XMLoadFloat4(&temp);

	DirectX::XMStoreFloat4(&temp, lb);
	toAffine(temp);
	const XMVECTOR b = DirectX::XMLoadFloat4(&temp);

	const XMVECTOR aSubB = a - b;
	const XMVECTOR bSubA = b - a;


	float left = DirectX::XMVectorGetX(DirectX::XMVector3Dot(a, aSubB)) / DirectX::XMVectorGetX(DirectX::XMVector3Dot(aSubB, aSubB));
	float right = DirectX::XMVectorGetX(DirectX::XMVector3Dot(b, bSubA)) / DirectX::XMVectorGetX(DirectX::XMVector3Dot(bSubA, bSubA));

	//const XMVECTOR l0 = (la*aSubB/(aSubB*aSubB))*lb + (lb*bSubA/ (bSubA*bSubA))*la;
	const XMVECTOR l0 = DirectX::XMVectorSetW(DirectX::XMVectorScale(b, left) + DirectX::XMVectorScale(a, right), 1);
	const XMVECTOR origin{ 0.0f, 0.0f, 0.0f, 1.0f };
	
	const XMMATRIX translation = translationMatrix(l0, origin);

	//return DirectX::XMMatrixIdentity();
	//return DirectX::XMMatrixRotationAxis(DirectX::XMVector3Normalize(bSubA), angle);
	return translation * DirectX::XMMatrixRotationAxis(aSubB, angle) * DirectX::XMMatrixInverse(nullptr, translation);
	//return DirectX::XMMatrixInverse(nullptr, translation) * DirectX::XMMatrixRotationAxis(DirectX::XMVector3Normalize(aSubB), angle) * translation;
}

void HypMat::toAffine(XMFLOAT4& v)
{
	v.x /= v.w;
	v.y /= v.w;
	v.z /= v.w;
	v.w = 1.0f;
}

void HypMat::normalize(DirectX::XMVECTOR & a)
{
	DirectX::XMFLOAT4 temp;
	DirectX::XMStoreFloat4(&temp, a);


	float f = std::sqrt(std::abs(temp.w*temp.w - temp.x*temp.x - temp.y*temp.y - temp.z*temp.z));
	temp.x /= f;
	temp.y /= f;
	temp.z /= f;
	temp.w /= f;
	a = XMLoadFloat4(&temp);
}
