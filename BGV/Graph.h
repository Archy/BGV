#pragma once
#include <string>
#include <memory>
#include <vector>
#include "Node.h"
#include "Model.h"
#include "BottomUpParser.h"
#include "TopDownParser.h"
#include "HypGraphRenderer.h"

class Graph
{
private:
	static constexpr double CUBE_WIDTH = 0.025;

private:
	std::shared_ptr<Node> root;
	std::vector<std::shared_ptr<Node>> nodes;

	std::vector<std::shared_ptr<AbstractParser>> parsers = {
		std::make_shared<BottomUpParser>(), 
		std::make_shared<TopDownParser>(),
		std::make_shared<HypGraphRenderer>()
	};

	std::vector<Model::InstancedData> instancedData;
	std::vector<Model::InstancedData> indicesInstancedData;

public:
	Graph();
	~Graph();

	void loadGraph(const std::string file);

	decltype(nodes.begin()) begin() { return nodes.begin(); }
	decltype(nodes.end()) end() { return nodes.end(); }
	decltype(nodes) getNodes() { return nodes; }


	const std::vector<Model::InstancedData>& getInstancedData();
	const std::vector<Model::InstancedData>& getIndicesInstancedData();

	decltype(nodes.size())  getChildrenNr() const{ return nodes.size(); }

	operator bool() const { return nodes.size() > 0; }

	static double getCubeWidth() { return CUBE_WIDTH; }
};

