#pragma once
#include <directxmath.h>

__declspec(align(16)) class Camera
{
public:
	Camera();
	~Camera();

	void* operator new(size_t i);
	void operator delete(void* p);

	void SetPosition(float x, float y, float z);
	void SetRotation(float x, float y, float z);

	void setLens(float width, float height);

	void Render();

	void GetViewMatrix(DirectX::XMMATRIX& viewMatrix) { viewMatrix = mViewMatrix; }
	void GetProjectionMatrix(DirectX::XMMATRIX &pojectionMatrix) { pojectionMatrix = mProjectionMatrix; }
	DirectX::XMMATRIX getViewProjMatrix() { return mViewMatrix*mProjectionMatrix; }

private:
	float mPositionX, mPositionY, mPositionZ;
	float mRotationX, mRotationY, mRotationZ;
	
	DirectX::XMMATRIX mViewMatrix;
	DirectX::XMMATRIX mProjectionMatrix;	//Macierz rzutowania perspektywicznego

	const float SCREEN_DEPTH;	// odleg�o�� bli�szej p�aszczyzny
	const float SCREEN_NEAR;	// odleg�o�� dalszej p�aszczyzny
};

