#pragma once
#include <string>
#include <memory>
#include <vector>
#include <DirectXMath.h>

class TopDownParser;

class Node
{
friend class TopDownParser;

private:
	std::string id;

	double x = 0.0, y = 0.0, z = 0.0, w=0.0;
	double radius = 0, theta = 0, phi = 0;

	int meshIndex;

	std::shared_ptr<Node> parent;
	std::vector<std::shared_ptr<Node>> children;

public:
	Node(std::string ID);
	~Node();

public: 
	std::string getID() { return id; }
	std::vector<std::shared_ptr<Node>>& getChildren() { return children; }
	std::shared_ptr<Node>& getParent(){ return parent; }
	void setParent(std::shared_ptr<Node> parent);

	void setRadius(double R) { radius = R; }
	double getRadius() const { return radius; }

	void setTheta(const double theta) 
	{
		this->theta = theta;
	}
	double getTheta() const
	{
		return theta;
	}
	void setPhi(const double phi)
	{
		this->phi = phi;
	}
	double getPhi() const
	{
		return phi;
	}

	void setPosition(float x, float y, float z, float w);
	double getX() const { return x; }
	double getY() const { return y; }
	double getZ() const { return z; }
	double getW() const { return w; }
	float getXFloat() const { return static_cast<float>(x); }
	float getYFloat() const { return static_cast<float>(y); }
	float getZFloat() const { return static_cast<float>(z); }
	float getWFloat() const { return static_cast<float>(w); }

	bool isLeaf() { return children.empty(); }
	decltype(children.size()) childrenSize() 
	{ 
		return children.size(); 
	}

	void convertToAffineCoords();

	DirectX::XMMATRIX getWorldMatrix() const {
		return DirectX::XMMATRIX{
			{ 1,0,0,0 },{ 0, 1, 0, 0 },{ 0, 0, 1, 0 },
			{ getXFloat(), getYFloat(), getZFloat(), 1 }, };
	}

	void setMeshIndex(int meshIndex) { this->meshIndex = meshIndex; }
	int getMeshIndex() { return meshIndex; }

};

