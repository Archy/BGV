#include "Graphics.h"
#include <memory>
#include <d3dcompiler.h>
#include "Constants.h"
#include "GameError.h"
#include <iostream>


Graphics::Graphics()
{
}


Graphics::~Graphics()
{
}

void * Graphics::operator new(size_t i)
{
	return _mm_malloc(i,16);
}

void Graphics::operator delete(void * p)
{
	_mm_free(p);
}

void Graphics::beginScene()
{
	mD3D->BeginScene();

	// Generate the view matrix based on the camera's position.
	mCamera->Render();

	mViewProjectionMatrix = mCamera->getViewProjMatrix(); //mViewMatrix*mProjectionMatrix;
}

void Graphics::endScene()
{
	mD3D->EndScene();
}

void Graphics::Render(const std::string &modelName, const DirectX::XMMATRIX& worldMatrix,
	int indexOffset, int vertexOffset, UINT indexCount)
{
	auto it = mModels.find(modelName);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << modelName << "\n";
		//TODO throw exception
		return;
	}

	//set nr of of indexes
	indexCount = (indexCount == 0 ? mModels[modelName]->GetIndexCount() : indexCount);

	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.	
	mModels[modelName]->Render(mD3D->GetDeviceContext());
	// Render the model using the color shader.
	try
	{
		mFX->Render(mD3D->GetDeviceContext(), indexCount,
			indexOffset, vertexOffset, worldMatrix, mViewProjectionMatrix);
	}
	catch (const GameError& e)
	{
		std::cout << "coudnt render model: " << modelName << "\n" << e.getMessage();
	}
}

void Graphics::Render(const std::string &modelName, int indexOffset, int vertexOffset,
	UINT indexCount)
{
	auto it = mModels.find(modelName);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << modelName << "\n";
		//TODO throw exception
		return;
	}

	//set nr of of indexes
	indexCount = (indexCount == 0 ? mModels[modelName]->GetIndexCount() : indexCount);

	DirectX::XMMATRIX worldMatrix;
	// Get the world, view, and projection matrices from the camera and d3d objects.
	mD3D->GetWorldMatrix(worldMatrix);


	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.	
	mModels[modelName]->Render(mD3D->GetDeviceContext());
	// Render the model using the color shader.
	try
	{
		mFX->Render(mD3D->GetDeviceContext(), indexCount,
			indexOffset, vertexOffset, worldMatrix, mViewProjectionMatrix);
	}
	catch (const GameError& e)
	{
		std::cout << "coudnt render model: " << modelName << "\n" << e.getMessage();
	}

}

void Graphics::Init(const HWND hWnd)
{
	//DirectX init
	mD3D = std::make_unique<D3DClass>();
	mD3D->InitD3D(hWnd, mClientWidth, mClientHeight);

	mFX = std::make_unique<ColorShader>();
	mFX->Initialize(mD3D->GetDevice(), "basicShader.fx");

	mCamera = std::make_unique<MovingCamera>();
	mCamera->setLens(mClientHeight, mClientHeight);
}

void Graphics::rotateCamera(const float dTheta, float dPhi)
{
	mCamera->rotate(dTheta, dPhi);
}

void Graphics::moveCamera(const float dRadius)
{
	mCamera->move(dRadius);
}

std::shared_ptr<Model> Graphics::addModel(const std::string &name,
	const std::vector<Model::Vertex> &vertices, const std::vector<UINT> &indices)
{
	auto pos = mModels.find(name);
	if (pos != mModels.end())
	{
		//TODO throw exception
		return nullptr;
	}

	std::shared_ptr<Model> tempModel = std::make_shared<Model>();
	tempModel->Initialize(mD3D->GetDevice(), vertices, indices);
	mModels.emplace(name, tempModel);

	return tempModel;
}

void Graphics::setClientSize(const int width, const int height)
{
	mClientHeight = height;
	mClientWidth = width;
}

void Graphics::resize()
{
	mD3D->Resize(mClientWidth, mClientHeight);
	mCamera->setLens(mClientWidth, mClientHeight);
}

void Graphics::setRasterizerStateSolid()
{
	mD3D->setRasterizerStateSolid();
}

void Graphics::setRasterizerStateWireframe()
{
	mD3D->setRasterizerStateWireframe();
}