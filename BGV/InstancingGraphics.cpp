
#include "InstancingGraphics.h"
#include <iostream>

InstancingGraphics::InstancingGraphics()
{
}


InstancingGraphics::~InstancingGraphics()
{
}

void InstancingGraphics::Init(const HWND hWnd)
{
	Graphics::Init(hWnd);

	mInstancingShader = std::make_unique<InstancingShader>();
	mInstancingShader->Initialize(mD3D->GetDevice(), "instancingShader.fx");
}

void InstancingGraphics::prepareInstancing(const std::string &modelName)
{
	std::shared_ptr<InstancingModel> iModel;

	if (findInstancingModel(modelName, iModel))
	{
		// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.	
		iModel->Render(mD3D->GetDeviceContext());
		mCurrentModelName = modelName;
	}
	else
	{
		std::cout << "Model: " << modelName << " is not an instancing model" << std::endl;
		//TODO throw exception
	}

	return;
}

void InstancingGraphics::RenderCurrentInstance(const DirectX::XMMATRIX& worldMatrix,
	int indexOffset, int vertexOffset, UINT indexCount)
{
	//set nr of of indices
	indexCount = (indexCount == 0 ? mModels[mCurrentModelName]->GetIndexCount() : indexCount);

	try
	{
		mFX->Render(mD3D->GetDeviceContext(), indexCount,
			indexOffset, vertexOffset, worldMatrix, mViewProjectionMatrix);
	}
	catch (const GameError& e)
	{
		std::cout << "coudnt render model: " << mCurrentModelName << "\n" << e.getMessage();
	}
}

void InstancingGraphics::RenderCurrentInstance(int instanceCount, int instanceOffset,
	int indexOffset, int vertexOffset, UINT indexCount)
{
	//set nr of of indexes
	indexCount = (indexCount == 0 ? mModels[mCurrentModelName]->GetIndexCount() : indexCount);

	mInstancingShader->Render(mD3D->GetDeviceContext(),
		indexCount, instanceCount, instanceOffset,
		indexOffset, vertexOffset, mViewProjectionMatrix);
}

void InstancingGraphics::RenderCurrentInstanceLine(int instanceCount, int instanceOffset, int vertexOffset)
{
	mInstancingShader->RenderLine(mD3D->GetDeviceContext(), instanceCount, instanceOffset, vertexOffset, mViewProjectionMatrix);
}

std::shared_ptr<Model> InstancingGraphics::addInstancingModel(const std::string &name, 
	const std::vector<Model::Vertex>& vertices, const std::vector<UINT>& indices)
{
	auto pos = mModels.find(name);
	if (pos != mModels.end())
	{
		//TODO throw exception
		return nullptr;
	}

	std::shared_ptr<InstancingModel> tempModel = std::make_shared<InstancingModel>();
	tempModel->Initialize(mD3D->GetDevice(), vertices, indices);
	mModels.emplace(name, tempModel);

	return tempModel;
}

std::shared_ptr<Model> InstancingGraphics::addInstancinLine(const std::string & name, 
	const std::vector<Model::Vertex>& vertices)
{
	auto pos = mModels.find(name);
	if (pos != mModels.end())
	{
		//TODO throw exception
		return nullptr;
	}

	std::shared_ptr<InstancingLine> tempModel = std::make_shared<InstancingLine>();
	tempModel->Initialize(mD3D->GetDevice(), vertices);
	mModels.emplace(name, tempModel);

	return tempModel;
}

void InstancingGraphics::createInstancedBuffer(const std::vector<Model::InstancedData>& instancedData, std::string model)
{
	std::shared_ptr<InstancingModel> iModel;
	if (findInstancingModel(model, iModel))
	{
		iModel->createInstancedBuffer(mD3D->GetDevice(), instancedData);
	}
	else
	{
		//TODO throw exception or sth
	}
}

bool InstancingGraphics::findInstancingModel(const std::string &name, std::shared_ptr<InstancingModel> &model)
{
	auto it = mModels.find(name);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << name << "\n";
		return false;
	}


	std::shared_ptr<InstancingModel> iModel;
	return (model = std::dynamic_pointer_cast<InstancingModel>(mModels[name])).operator bool();

}

bool InstancingGraphics::findInstancingLine(const std::string & name, std::shared_ptr<InstancingLine>& model)
{
	auto it = mModels.find(name);
	if (it == mModels.end())
	{
		std::cout << "No such model: " << name << "\n";
		return false;
	}


	std::shared_ptr<InstancingModel> iModel;
	return (model = std::dynamic_pointer_cast<InstancingLine>(mModels[name])).operator bool();

}
