#include <memory>
#include "Model.h"


Model::Model() :
	mIB(nullptr), mVB(nullptr)
{
}


Model::~Model()
{
}

void Model::Initialize(CComPtr<ID3D11Device> dev, const std::vector<Vertex> &vertices,
	const std::vector<UINT> &indices)
{
	HRESULT result;
	const Vertex *verticesBegin = &vertices[0];
	const UINT *indicesBegin = &indices[0];
	
	mIndexCount = static_cast<int>(indices.size());
	mVertexCount = static_cast<int>(vertices.size());

	D3D11_BUFFER_DESC bDesc;
	bDesc.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
	bDesc.Usage = D3D11_USAGE_IMMUTABLE; //zawartosc bufora nie zmienia sie 
										 //po jego utworzeniu
	bDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bDesc.CPUAccessFlags = 0;
	bDesc.MiscFlags = 0;
	bDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = verticesBegin;

	result =dev->CreateBuffer(
		&bDesc, // opis tworzonego bufora
		&initData, // dane s�u��ce do inicjalizacji bufora
		&mVB); // zwraca tworzony bufor
	if (FAILED(result)) {
		throw GameError(FATAL_ERROR, "Error creating vertex buffer");
	}

	// Create the index buffer
	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = static_cast<UINT>(sizeof(UINT) * indices.size());
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = indicesBegin;
	result = dev->CreateBuffer(&ibd, &iinitData, &mIB);
	if (FAILED(result))
	{
		throw GameError(FATAL_ERROR, "Error creating buffer");
	}
}

void Model::Render(CComPtr<ID3D11DeviceContext>& devCon)
{
	// Set vertex buffer stride and offset.
	unsigned int stride = sizeof(Vertex);
	unsigned int offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	devCon->IASetVertexBuffers(0, 1, &(mVB.p), &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	devCon->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	devCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}