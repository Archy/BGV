#include "InstancingShader.h"
#include <iostream>

InstancingShader::InstancingShader()
{
}

InstancingShader::~InstancingShader()
{
}

void InstancingShader::Render(CComPtr<ID3D11DeviceContext> deviceContext,
	int indexCount, int InstanceCount, int instanceOffset, int indexOffset, int vertexOffset,
	const DirectX::XMMATRIX & viewProjectionMatrix)
{
	bool result;
	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, viewProjectionMatrix);
	if (!result)
	{
		//throw some exception
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount, InstanceCount, instanceOffset, indexOffset, vertexOffset);
}

void InstancingShader::RenderLine(CComPtr<ID3D11DeviceContext> deviceContext, int InstanceCount, int instanceOffset, int vertexOffset, const DirectX::XMMATRIX & viewProjectionMatrix)
{
	bool result;
	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, viewProjectionMatrix);
	if (!result)
	{
		//throw some exception
	}

	// Now render the prepared buffers with the shader.
	RenderShaderNonIndexed(deviceContext, InstanceCount, instanceOffset, vertexOffset);
}

void InstancingShader::createLayout(CComPtr<ID3D11Device> device)
{
	HRESULT result;
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },

		//TODO set optimal number of passed instances
		{ "WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0,
		D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16,
		D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32,
		D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48,
		D3D11_INPUT_PER_INSTANCE_DATA, 1 },

		{ "COLORTWO", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 64,
		D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};

	D3DX11_PASS_DESC passDesc;
	mTech->GetPassByIndex(0)->GetDesc(&passDesc);
	result = device->CreateInputLayout(vertexDesc, 7, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &mLayout);

	if (FAILED(result)) {
		std::cout << "Error creating input layout: " << result << std::endl;
		std::string msg = "Error creating input layout: " + result;
		throw GameError(FATAL_ERROR, msg);
	}
}

bool InstancingShader::SetShaderParameters(CComPtr<ID3D11DeviceContext> deviceContext,
	const DirectX::XMMATRIX &viewProjectionMatrix)
{

	// Set the vertex input layout.
	//	deviceContext->IASetInputLayout(mLayout);
	deviceContext->IASetInputLayout(mLayout);

	DirectX::XMMATRIX worldViewProj;
	worldViewProj = viewProjectionMatrix;
	mfxWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));

	return true;
}

void InstancingShader::RenderShader(CComPtr<ID3D11DeviceContext> deviceContext,
	int indexCount, int InstanceCount, int instanceOffset,
	int indexOffset, int vertexOffset)
{
	D3DX11_TECHNIQUE_DESC techDesc;
	mTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex(p)->Apply(0, deviceContext);

		deviceContext->DrawIndexedInstanced(indexCount, InstanceCount,
			indexOffset, vertexOffset, instanceOffset);
	}
}

void InstancingShader::RenderShaderNonIndexed(CComPtr<ID3D11DeviceContext> deviceContext, int InstanceCount, int instanceOffset, int vertexOffset)
{
	UINT VertexCountPerInstance = 4; //for line; TODO add as call parameter

	D3DX11_TECHNIQUE_DESC techDesc;
	mTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex(p)->Apply(0, deviceContext);

		deviceContext->DrawInstanced(VertexCountPerInstance, InstanceCount,
			vertexOffset, instanceOffset);
	}
}
