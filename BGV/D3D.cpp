#include "D3D.h"
#include "Constants.h"
#include "GameError.h"
#include <iostream>


D3DClass::D3DClass() : 
	mSwapchain(nullptr), mDev(nullptr), mDevCon(nullptr), mBackBufferView(nullptr),
	mEnable4xMsaa(false), mVsync(false)
{
}

D3DClass::~D3DClass()
{
}

void * D3DClass::operator new(size_t i)
{
	return _mm_malloc(i,16);
}

void D3DClass::operator delete(void * p)
{
	_mm_free(p);
}

void D3DClass::InitD3D(HWND hWnd, const int width, const int height)
{
	//Create D3Device
	this->createDeviceAndContext(hWnd);
	//Create SwapChain
	this->createSwapchain(hWnd, width, height);
	//Setting the Render Target
	this->setRenderTarget();
	//create depth/stencil buffer
	this->createDepthStencilBuffer(width, height);
	// set the render target as the back buffer
	this->mDevCon->OMSetRenderTargets(1, &(mBackBufferView.p), mDepthStencilView);
	// Set the viewport
	this->setViewpoint(width, height);
	//create Rasterizer State
	createRasterizerStateSolid();
	createRasterizerStateWireframe();

	setMatrices(width, height);
}

void D3DClass::Resize(const int width, const int height)
{
	//release old views
	mBackBufferView.Release();
	mDepthStencilBuffer.Release();
	mDepthStencilView.Release();

	//resize swapchain
	auto result = mSwapchain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	if (FAILED(result)) 
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error resizing swapchain");
	}

	//recreate target view
	this->setRenderTarget();
	//recreate depth/stencil buffer
	this->createDepthStencilBuffer(width, height);
	//reset the render target as the back buffer
	this->mDevCon->OMSetRenderTargets(1, &(mBackBufferView.p), mDepthStencilView);
	//reset the viewport
	this->setViewpoint(width, height);
	//reset matrices
	setMatrices(width, height);
}

void D3DClass::BeginScene()
{
	// Clear the back buffer.
	mDevCon->ClearRenderTargetView(mBackBufferView, reinterpret_cast<const float*>(&Colors::LightGrey));
	// Clear the depth buffer.

	//TODO: THROWS EXCEPTION WHEN MOVING WINDOW
	mDevCon->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	mDevCon->RSSetState(mRasterStateSolid);
}

void D3DClass::EndScene()
{
	// Present the back buffer to the screen since rendering is complete.
	if (mVsync)
	{
		// Lock to screen refresh rate.
		mSwapchain->Present(1, 0);
	}
	else
	{
		// Present as fast as possible.
		mSwapchain->Present(0, 0);
	}
}

void D3DClass::GetWorldMatrix(DirectX::XMMATRIX & worldMatrix)
{
	worldMatrix = mWorldMatrix;
}

void D3DClass::GetOrthoMatrix(DirectX::XMMATRIX & orthoMatrix)
{
	orthoMatrix = mOrthoMatrix;
}

void D3DClass::setRasterizerStateSolid()
{
	mDevCon->RSSetState(mRasterStateSolid);
}

void D3DClass::setRasterizerStateWireframe()
{
	mDevCon->RSSetState(mRasterStateWireframe);
}

void D3DClass::createDeviceAndContext(const HWND hWnd)
{
	HRESULT result;
	auto featureLevel = D3D_FEATURE_LEVEL_11_0; //computer must support dx11	
	UINT createDeviceFlags = 0; 
	// Flag  'D3D11_CREATE_DEVICE_SINGLETHREADED' improves performance if if D3D wont be called from multiple threads
#if defined(DEBUG) || defined(_DEBUG)
	//flag for debug - d3d will send communicates to vs console
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// create a device and device context
	result = D3D11CreateDevice(
		NULL,	//default card
		D3D_DRIVER_TYPE_HARDWARE, //use 3D hardware acceleration
		NULL,
		createDeviceFlags,
		&featureLevel,
		1,
		D3D11_SDK_VERSION,
		&this->mDev,			//function returns device, its context and feature level
		&this->mFeatureLevel,
		&this->mDevCon);

	if (FAILED(result))
	{
		result = D3D11CreateDevice(
			NULL,	//default card
			D3D_DRIVER_TYPE_REFERENCE,	//software implementation of Direct3D with goal of correctness(part of DX SDK)
			NULL,
			createDeviceFlags,
			&featureLevel,
			1,
			D3D11_SDK_VERSION,
			&this->mDev,
			&this->mFeatureLevel,
			&this->mDevCon);
		if (FAILED(result)) {
			MessageBox(NULL, "Graphic card does not suppert DX11. Reference device not available", "Fatal Error", MB_OK);
			throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D device - D3D_FEATURE_LEVEL_11_0 not available");
		}
		else {
			MessageBox(NULL, "Graphic card does not suppert DX11. All graphic will be handled by software implementation of D3D", "Warning", MB_OK);
		}
	}
}

void D3DClass::createSwapchain(const HWND hWnd, const int width, const int height)
{
	HRESULT result;
	DXGI_SWAP_CHAIN_DESC sd;
	UINT m4xMsaaQuality;
	
	//check quality level support for 4xMSAA
	result = mDev->CheckMultisampleQualityLevels(
		DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);
	assert(m4xMsaaQuality > 0); //4xMSAA is always supported in DX11

	// create a struct to hold information about the swap chain
	// fill the swap chain description struct
	sd.BufferDesc.Width = width; // use size from clients window
	sd.BufferDesc.Height = height;
	sd.BufferDesc.RefreshRate.Numerator = 60; 
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //24bit color is enaught for most sreens
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; //backbufer will be used for rendering
	sd.BufferCount = 1; //nr of backbufers
	sd.OutputWindow = hWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; //allow 
	sd.Flags = 0;
	if (mEnable4xMsaa) // antialiasing with 4 time sampling
	{
		sd.SampleDesc.Count = 4;
		sd.SampleDesc.Quality = m4xMsaaQuality - 1;
	}
	else //no antialiasing
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
	}

	//Create Swapchain:
	//DXGI - separete API for handling graphic related things like the swap chain

	//get IDXGIFactory device
	CComPtr<IDXGIDevice> dxgiDevice = 0;
	result = mDev->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain - Failed to get IDXGIFactory device");
	}
	//get IDXGIFactory adapter
	CComPtr<IDXGIAdapter> dxgiAdapter = 0;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain - Failed to get IDXGIFactory adapter");
	}

	//finally get IDXGIFactory interface
	CComPtr<IDXGIFactory> dxgiFactory = 0;
	result = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain - Failed to get IDXGIFactory interface");
	}
	// create swapchain
	result = dxgiFactory->CreateSwapChain(mDev, &sd, &mSwapchain);
	if (FAILED(result)) {
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Direct3D swapchain");
	}
}

void D3DClass::createDepthStencilBuffer(const int width, const int height)
{
	UINT m4xMsaaQuality;
	HRESULT result;
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	

	result = mDev->CheckMultisampleQualityLevels(
		DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);

	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT; //only gpu can write/read from resource (cpu cant)
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;
	// antialiasing with 4 time sampling
	if (mEnable4xMsaa)
	{
		depthStencilDesc.SampleDesc.Count = 4;
		depthStencilDesc.SampleDesc.Quality = m4xMsaaQuality - 1;
	}
	else
	{
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}
	
	result = mDev->CreateTexture2D(
		&depthStencilDesc, // describes texture being created
		0,	//no need to initial filling of texture
		&mDepthStencilBuffer); // returns pointer to depth/stencil buffer
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating depth/stencil bufer");
	}
	result = mDev->CreateDepthStencilView(
		mDepthStencilBuffer, // resource for which view is created
		0, //type of view(already specified -> null)
		&mDepthStencilView); // returns view of depth/stencil
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating depth/stencil bufer");
	}
}

void D3DClass::setViewpoint(const int width, const int height)
{
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = static_cast<float>(width);
	viewport.Height = static_cast<float>(height);
	viewport.MinDepth = 0.0; //min/max value of depth buffer
	viewport.MaxDepth = 1.0;

	this->mDevCon->RSSetViewports(1, &viewport);
}

void D3DClass::setRenderTarget()
{
	// get the address of the back buffer
	HRESULT result;
	CComPtr<ID3D11Texture2D> pBackBuffer;

	result = this->mSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error getting backbufer");
	}

	// use the back buffer address to create the render target
	this->mDev->CreateRenderTargetView(pBackBuffer, NULL, &mBackBufferView);
	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating render target view");
	}
}

void D3DClass::setMatrices(const int width, const int height)
{	
	// Initialize the world matrix to the identity matrix.
	mWorldMatrix = DirectX::XMMatrixIdentity();
}

void D3DClass::createRasterizerStateSolid()
{
	HRESULT result;

	D3D11_RASTERIZER_DESC wireframeDesc;
	ZeroMemory(&wireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	wireframeDesc.FillMode = D3D11_FILL_SOLID;
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FrontCounterClockwise = false;
	wireframeDesc.DepthClipEnable = true;

	result = mDev->CreateRasterizerState(&wireframeDesc, &mRasterStateSolid);

	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Rasterizer State");
	}
}

void D3DClass::createRasterizerStateWireframe()
{
	HRESULT result;

	D3D11_RASTERIZER_DESC wireframeDesc;
	ZeroMemory(&wireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	wireframeDesc.FillMode = D3D11_FILL_WIREFRAME;
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FrontCounterClockwise = false;
	wireframeDesc.DepthClipEnable = true;

	result = mDev->CreateRasterizerState(&wireframeDesc, &mRasterStateWireframe);

	if (!SUCCEEDED(result))
	{
		throw GameError(GameErrorsList::D3D_INIT_ERROR, "Error creating Rasterizer State");
	}
}