#include "Node.h"



Node::Node(std::string ID) :
	id(ID)
{
	radius = 0.1;
}


Node::~Node()
{
}

void Node::setParent(std::shared_ptr<Node> parent)
{
	parent = parent;
}

void Node::setPosition(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

void Node::convertToAffineCoords()
{
	x /= w;
	y /= w;
	z /= w;
}
