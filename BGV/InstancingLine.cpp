#include "InstancingLine.h"
#include <iostream>

InstancingLine::InstancingLine()
{
}


InstancingLine::~InstancingLine()
{
}

void InstancingLine::Initialize(CComPtr<ID3D11Device> dev, const std::vector<Vertex>& vertices)
{
	HRESULT result;
	const Vertex *verticesBegin = &vertices[0];
	mVertexCount = static_cast<int>(vertices.size());

	//create only vertex buffor
	D3D11_BUFFER_DESC bDesc;
	bDesc.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
	bDesc.Usage = D3D11_USAGE_IMMUTABLE; //zawartosc bufora nie zmienia sie 
										 //po jego utworzeniu
	bDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bDesc.CPUAccessFlags = 0;
	bDesc.MiscFlags = 0;
	bDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = verticesBegin;

	result = dev->CreateBuffer(
		&bDesc, // opis tworzonego bufora
		&initData, // dane s�u��ce do inicjalizacji bufora
		&mVB); // zwraca tworzony bufor
	if (FAILED(result)) {
		throw GameError(FATAL_ERROR, "Error creating vertex buffer");
	}
}

void InstancingLine::Render(CComPtr<ID3D11DeviceContext>& devCon)
{
	// Set vertex buffer stride and offset.
	unsigned int stride[2] = { sizeof(Vertex), sizeof(InstancedData) };
	unsigned int offset[2] = { 0,0 };

	ID3D11Buffer* vbs[2] = { (mVB.p), (mInstancedBuffer.p) };

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	devCon->IASetVertexBuffers(0, 2, vbs, stride, offset);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	devCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	return;
}
