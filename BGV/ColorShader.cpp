#include "ColorShader.h"
#include <iostream>
#include <iomanip>

ColorShader::ColorShader()
{
}

ColorShader::~ColorShader()
{
}


void ColorShader::Render(CComPtr<ID3D11DeviceContext> deviceContext,
	int indexCount, int indexOffset, int vertexOffset, 
	const DirectX::XMMATRIX &worldMatrix, const DirectX::XMMATRIX &viewProjectionMatrix)
{
	bool result;
	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, worldMatrix, viewProjectionMatrix);
	if (!result)
	{
		//throw some exception
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount, indexOffset, vertexOffset);
}

void ColorShader::createLayout(CComPtr<ID3D11Device> device)
{
	HRESULT result;
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12,
		D3D11_INPUT_PER_VERTEX_DATA, 0 },

	    {"WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, 
		 D3D11_INPUT_PER_INSTANCE_DATA, 1},
	};

	D3DX11_PASS_DESC passDesc;
	mTech->GetPassByIndex(0)->GetDesc(&passDesc);
	result = device->CreateInputLayout(vertexDesc, 2, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &mLayout);
	if (FAILED(result)) {
		std::cout << "Error creating COLOR input layout: " << std::hex << result << std::endl;
		throw GameError(FATAL_ERROR, "Error creating COLOR input layout");
	}
}

bool ColorShader::SetShaderParameters(CComPtr<ID3D11DeviceContext> deviceContext,
	const DirectX::XMMATRIX &worldMatrix, const DirectX::XMMATRIX &viewProjectionMatrix)
{

	// Set the vertex input layout.
	deviceContext->IASetInputLayout(mLayout);

	DirectX::XMMATRIX worldViewProj;
	worldViewProj = worldMatrix*viewProjectionMatrix;
	mfxWorldViewProj->SetMatrix(reinterpret_cast<float*>(&worldViewProj));
	
	return true;
}

void ColorShader::RenderShader(CComPtr<ID3D11DeviceContext> deviceContext,
	int indexCount, int indexOffset, int vertexOffset)
{
	D3DX11_TECHNIQUE_DESC techDesc;
	mTech->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex(p)->Apply(0, deviceContext);
		deviceContext->DrawIndexed(indexCount, indexOffset, vertexOffset);
	}
}