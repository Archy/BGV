#pragma once
#include <memory>
#include <vector>
#include <DirectXMath.h>
#include "Node.h"
#include "Constants.h"
#include "LevelRenderer.h"

class GraphRenderer
{
public:
	GraphRenderer(const double cubeWidth);
	~GraphRenderer();

	void renderGraph(std::shared_ptr<Node> &root);

private:
	void renderSubnodeLeafs(std::shared_ptr<Node> &n, DirectX::XMVECTOR dir, double R);
	void renderNode(std::shared_ptr<Node> &n, DirectX::XMVECTOR dir, DirectX::XMVECTOR vertShift);

	const double CUBE_WIDTH;

private:
	class SphereDistributor
	{
	private:
		using Point = DirectX::XMFLOAT2;
		using List = std::vector<Point>;
	public:

		SphereDistributor()
		{
			//n = 1
			mDist.push_back(List());
			mDist[0].push_back(Point(0, 0));

			//n = 2
			mDist.push_back(List());
			mDist[1].push_back(Point(0, 0));
			mDist[1].push_back(Point(PI, 0));

			//n = 3
			mDist.push_back(List());
			mDist[2].push_back(Point(0, 0));
			mDist[2].push_back(Point(2.0*PI / 3.0, 0));
			mDist[2].push_back(Point(2.0 * PI / 3.0, PI));

			//n = 4
			mDist.push_back(List());
			mDist[3].push_back(Point(0, 0));
			mDist[3].push_back(Point(2.0 * PI / 3.0, 0));
			mDist[3].push_back(Point(2.0 * PI / 3.0, 2.0 *PI / 3.0));
			mDist[3].push_back(Point(2.0 * PI / 3.0, 4.0* PI / 3.0));

			// n = 5
			mDist.push_back(List());
			mDist[4].push_back(Point(PI / 3.0, 0));
			mDist[4].push_back(Point(PI / 3.0, PI));
			mDist[4].push_back(Point(2.0 * PI / 3.0, 0));
			mDist[4].push_back(Point(2.0 * PI / 3.0, 2.0 * PI / 3.0));
			mDist[4].push_back(Point(2.0 * PI / 3.0, 4.0 * PI / 3.0));

			// n = 6
			mDist.push_back(List());
			mDist[5].push_back(Point(PI / 4.0, 0));
			mDist[5].push_back(Point(PI / 4.0, 2.0 * PI / 3.0));
			mDist[5].push_back(Point(PI / 4.0, 4.0 * PI / 3.0));
			mDist[5].push_back(Point(PI / 4.0 + 2.0 * PI / 4.0, 0));
			mDist[5].push_back(Point(PI / 4.0 + 2.0 * PI / 4.0, 2.0 * PI / 3.0));
			mDist[5].push_back(Point(PI / 4.0 + 2.0 * PI / 4.0, 4.0 * PI / 3.0));
		}

		List& getDistribution(int n)
		{
			return mDist[n - 1];
		}

	private:
		std::vector<List> mDist;
	};
};
