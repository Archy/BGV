#pragma once
#include "AbstractParser.h"
class TopDownParser : public AbstractParser
{
public:
	TopDownParser();
	~TopDownParser();

	virtual void parse(const std::shared_ptr<Node>& root) override;

private:
	void countAngles(const std::shared_ptr<Node>& node);
	void placeKids(const std::shared_ptr<Node>& node);
	void setPositions(const std::shared_ptr<Node>& node);

	void sortChildren(const std::shared_ptr<Node>& node);

	double computeDeltaPhi(const double nodeRadius, const double parentRadius);
	double computeDeltaTheta(const double nodeRadius, const double parentRadius, double phi);
};

