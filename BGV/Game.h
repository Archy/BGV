#pragma once
#include <Windows.h>
#include <memory>
#include <string>

#include "GameError.h"
#include "Constants.h"

#include "InstancingGraphics.h"
#include "InputHandler.h"
#include "GameTimer.h"


class Game
{
public:
	Game();
	virtual ~Game();

	// Window message handler
	LRESULT MessageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	// Initialize the game
	// Pre: hwnd is handle to window
	//throws GameError on failure
	virtual void Initialize(HWND hwnd);
	// Call run repeatedly by the main message loop in WinMain
	virtual void Frame();
	
	//if game is running
	bool IsRunning() { return mIsRunning; }
	//finish game
	void quit() { mIsRunning = false; }



protected:	
	virtual void Update() = 0;
	virtual void Render() = 0;

	void CalculateFrameStats();

protected:
	std::unique_ptr<InstancingGraphics> mGraphic;
	std::unique_ptr<InputHandler> mInput;
	std::unique_ptr<GameTimer> mTimer;

private:
	bool mIsRunning;
	bool mPaused;
	bool mResizing = false;

	HWND mHwnd;
};