#include "Graph.h"
#include <iostream>
#include "GraphParser.h"

Graph::Graph()
{
}


Graph::~Graph()
{
}

void Graph::loadGraph(const std::string file)
{
	GraphParser parser;
	if (parser.loadGraph(file, root, nodes))
	{
		std::cout << "Graph parsed" << std::endl;
		for (auto parser : parsers)
		{
			parser->parse(root);
		}
	}

}

const std::vector<Model::InstancedData>& Graph::getInstancedData()
{
	DirectX::XMMATRIX world;

	int meshIndex = 0;
	for (auto &node : nodes)
	{
		node->convertToAffineCoords();
		world = DirectX::XMMATRIX{
			{ 1,0,0,0 },{ 0, 1, 0, 0 },{ 0, 0, 1, 0 },
			{ node->getXFloat(), node->getYFloat(), node->getZFloat(), 1 }, };

		instancedData.push_back(Model::InstancedData(world, Colors::White));
		node->setMeshIndex(meshIndex++);
	}

	return instancedData;
}

const std::vector<Model::InstancedData>& Graph::getIndicesInstancedData()
{
	DirectX::XMMATRIX world;

	DirectX::XMMATRIX scaleMatrix;
	DirectX::XMMATRIX rotationMatrix;
	DirectX::XMMATRIX translationMatrix;

	//for computing line length
	float scaleX;
	//for computing line rotation
	DirectX::XMVECTOR axis;
	float angle;
	//used when child-parent vector and line vector
	//are perpendicular
	DirectX::XMVECTOR n, c;
	
	//the vector between parent node and children
	DirectX::XMVECTOR dest;
	//vector of line model
	const DirectX::XMVECTOR basic = DirectX::XMVectorSet(1, 0, 0, 0);

	for (auto &node : nodes)
	{
		for (auto &child : node->getChildren())
		{
			//remember child-parent vector
			dest = DirectX::XMVectorSet(child->getX() - node->getX(),
				child->getY() - node->getY(), child->getZ() - node->getZ(), 0);

			//compute line scaling
			scaleX = DirectX::XMVectorGetByIndex(
				DirectX::XMVector3Length(dest), 0);
			scaleMatrix = DirectX::XMMatrixScaling(scaleX, 1, 1);

			//compute line rotation
			angle = DirectX::XMVectorGetByIndex(
				DirectX::XMVector3AngleBetweenVectors(dest, basic), 0);

			axis = DirectX::XMVector3Cross(basic, dest);
			//child-parent vector and line vector are perpendicular
			//no cross product!
			if (DirectX::XMVector3Equal(axis, DirectX::XMVectorZero()))
			{
				n = DirectX::XMVectorSet(node->getX(), node->getY(), node->getZ(), 0);
				c = DirectX::XMVectorSet(child->getX(), child->getY(), child->getZ(), 0);
				axis = DirectX::XMVectorSubtract(c, n);
			}
			rotationMatrix = DirectX::XMMatrixRotationAxis(axis, angle);

			//line shift
			translationMatrix = DirectX::XMMatrixTranslation(node->getX(), node->getY(), node->getZ());
			
			//create final transformation matrix 
			world = scaleMatrix * rotationMatrix * translationMatrix;
			indicesInstancedData.push_back(Model::InstancedData(world, Colors::Yellow));
		}
	}

	return indicesInstancedData;
}
