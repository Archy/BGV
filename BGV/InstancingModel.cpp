#include "InstancingModel.h"



InstancingModel::InstancingModel()
{

}


InstancingModel::~InstancingModel()
{
}


void InstancingModel::createInstancedBuffer(CComPtr<ID3D11Device> &dev,
	const std::vector<Model::InstancedData>& instancedData)
{
	//create buffor for instanced object data

	D3D11_BUFFER_DESC vbd;
	vbd.ByteWidth = static_cast<UINT>(sizeof(Model::InstancedData) * instancedData.size());
	vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	auto *dataBegin = &(instancedData[0]);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = dataBegin;

	dev->CreateBuffer(&vbd, &initData, &mInstancedBuffer);

	//mInstancedBuffer
}

void InstancingModel::changeColour(CComPtr<ID3D11DeviceContext>& devCon, 
									const int index, const DirectX::XMFLOAT4 instance)
{
	int aaa;

	aaa = 1;

	D3D11_MAPPED_SUBRESOURCE resource;
	ZeroMemory(&resource, sizeof(resource));

	devCon->Map(mInstancedBuffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &resource);
	BYTE *ptr = static_cast<BYTE*>(resource.pData);
	memcpy(( ptr + index*sizeof(InstancedData) + sizeof(DirectX::XMFLOAT4X4)), &instance, sizeof(DirectX::XMFLOAT4));
	devCon->Unmap(mInstancedBuffer, 0);
}


void InstancingModel::Render(CComPtr<ID3D11DeviceContext>& devCon)
{
	// Set vertex buffer stride and offset.
	unsigned int stride[2] = { sizeof(Vertex), sizeof(InstancedData) };
	unsigned int offset[2] = { 0,0 };

	ID3D11Buffer* vbs[2] = { (mVB.p), (mInstancedBuffer.p) };

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	devCon->IASetVertexBuffers(0, 2, vbs/*&(mVB.p)*/, stride, offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	devCon->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	devCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}
