#pragma once
#include <string>
#include <vector>
#include <memory>
#include <map>
#include "Node.h"
#include "rapidxml-1.13/rapidxml_utils.hpp"

class GraphParser
{
public:
	GraphParser();
	~GraphParser();

	bool loadGraph(std::string fileName, std::shared_ptr<Node>& root,
		std::vector<std::shared_ptr<Node>> &nodesVect);

private:
	void ParseNodes(std::shared_ptr<Node>& root, 
		std::vector<std::shared_ptr<Node>>& nodesVect);
	void ParseEdges(std::vector<std::shared_ptr<Node>>& nodesVect);

private:
	std::map<std::string, std::shared_ptr<Node>> mNodes;

	int mNodesNr = 0; 
	int mEdgesNr = 0;
	std::string mRootId;

	rapidxml::xml_node <>* mXmlRoot;
};

