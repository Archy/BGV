#pragma once
#include <Windows.h>
#include <d3d11.h>	//core pieces of Direct3D
#include <directxmath.h>

#define DEBUG

//-----------------------------------------------
//                  Constants
//-----------------------------------------------

// WNDOW
const char CLASS_NAME[] = "BGVMain";
const char GAME_TITLE[] = "BGV";
const bool FULLSCREEN = false;              // windowed or fullscreen
constexpr UINT  GAME_WIDTH = 640;              // width of game in pixels
constexpr UINT  GAME_HEIGHT = 480;              // height of game in pixels

//MATH
constexpr double PI = 3.14159265358979323846;

//DIRECTX
namespace Colors
{
	XMGLOBALCONST DirectX::XMFLOAT4 White = { 1.0f, 1.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Black = { 0.0f, 0.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Red = { 1.0f, 0.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Green = { 0.0f, 1.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Blue = { 0.0f, 0.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Yellow = { 1.0f, 1.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Cyan = { 0.0f, 1.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Magenta = { 1.0f, 0.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 Silver = { 0.75f, 0.75f, 0.75f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 LightSteelBlue = { 0.69f, 0.77f, 0.87f, 1.0f };

	XMGLOBALCONST DirectX::XMFLOAT4 LightSalmon = { 1.0f, 0.627f, 0.478f, 1.0f };
	XMGLOBALCONST DirectX::XMFLOAT4 LightGrey = { 0.827f, 0.827f, 0.827f, 1.0f };
}