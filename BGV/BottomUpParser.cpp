#include "BottomUpParser.h"
#include <cmath>
#include "Constants.h"

const double BottomUpParser::LEAF_RADIUS = 0.3;
const double BottomUpParser::MAX_RADIUS = 2.7;

BottomUpParser::BottomUpParser()
{
}


BottomUpParser::~BottomUpParser()
{
}

void BottomUpParser::parse(const std::shared_ptr<Node>& root)
{
	countRadius(root);
}

double BottomUpParser::countRadius(const std::shared_ptr<Node>& node)
{
	if (node->isLeaf())
	{
		node->setRadius(LEAF_RADIUS);
		return LEAF_RADIUS;
	}

	double HA = 0.0;
	for (auto child : node->getChildren())
	{
		HA += 2.0*PI*(std::cosh(countRadius(child)) - 1.0);
	}

	if (node->childrenSize() == 1) {
		HA += HA*std::tanh(HA)*0.2;
	}
	else if (node->childrenSize() == 2) {
		HA += HA*std::tanh(HA)*0.9;
	}
	else if (node->childrenSize() > 2 && node->childrenSize() < 6) {
		HA += HA*std::tanh(HA)*0.6;
	}
	else {
		HA += HA*std::tanh(HA)*0.4;
	}

	double radius = std::asinh(std::sqrt(HA/(2.0*PI)));
	if (radius < LEAF_RADIUS)
		radius = LEAF_RADIUS;
	if (radius > MAX_RADIUS)
		radius = MAX_RADIUS;

	node->setRadius(radius);
	return radius;
}
