#pragma once
#include "Game.h"
#include "Graph.h"

class Box : public Game
{
public:
	Box();
	~Box();

	virtual void Initialize(HWND hwnd);

protected:
	virtual void Update();
	virtual void Render();

private:
	void createBox();
	void createLine();

private:
	std::unique_ptr<Graph> mGraph; 

	POINT mMouse;
};

