#pragma once
#include "InstancingModel.h"
class InstancingLine :
	public InstancingModel
{
public:
	InstancingLine();
	~InstancingLine();

	virtual void Initialize(CComPtr<ID3D11Device> dev,
		const std::vector<Vertex> &vertices);

	virtual void Render(CComPtr<ID3D11DeviceContext> &devCon);
};

