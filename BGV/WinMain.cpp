#include <Windows.h>
#include <memory>
#include <iostream>
#include "Box.h"

// Function prototypes
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow);
bool AnotherInstance();
bool createConsole();
bool CreateMainWindow(HWND &hwnd, HINSTANCE hInstance, int nCmdShow);
void clearWindow(HWND &hwnd, HINSTANCE hInstance);
LRESULT WINAPI WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//Global variables
//the game
std::unique_ptr <Game> GAME;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow) 
{
	MSG	 msg;
	HWND hwnd = NULL;

	// Prevent Multiple Instances
	if (AnotherInstance())
	{
		MessageBox(NULL, "Game already running", "Error", MB_OK);
		return 0;
	}

	//creates console
	if (createConsole() == false)
	{
		MessageBox(NULL, "Console was not created", "Warning", MB_OK);
	}

	
	//create game
	GAME = std::make_unique<Box>();
	// Create the window
	if (!CreateMainWindow(hwnd, hInstance, nCmdShow))
		return 1;
	try
	{
		// Create Graphics object
		GAME->Initialize(hwnd);
		std::cout << "Game init success!" << std::endl;
		// Main message loop
		while (GAME->IsRunning())
		{
			// PeekMessage is a non-blocking method for checking for Windows messages
			// GetMessage waits for messages before returning
			//Critical for animation
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				// Look for quit message
				if (msg.message == WM_QUIT)
					GAME->quit();
				// Decode and pass messages on to WinProc
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
				GAME->Frame(); // Run the game loop
		}
	}
	catch (const GameError& err)
	{
		MessageBox(NULL, err.getMessage(), "Game Error", MB_OK);
	}
	catch (std::exception& err)
	{
		std::cout<< "Error: " << err.what() << std::endl;
	}
	catch (...)
	{
		MessageBox(NULL, "Unknown error occurred in game.", "Unknown Error", MB_OK);
	}

	GAME.release();
	FreeConsole();
	clearWindow(hwnd, hInstance);
	return 0;
}


//========================================================================
// Window event callback function
//========================================================================
LRESULT WINAPI WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return (GAME->MessageHandler(hWnd, msg, wParam, lParam));
}

//=============================================================================
// Create the window
// Returns: false on error
//=============================================================================
bool CreateMainWindow(HWND &hwnd, HINSTANCE hInstance, int nCmdShow)
{
	//=============================================================================
	//client area - inside part of the window where our game graphics are drawn
	//=============================================================================

	WNDCLASSEX wcx;

	// Fill in the Window class structure with parameters
	// that describe the main window.
	wcx.cbSize = sizeof(wcx); // Size of structure
	wcx.style = CS_HREDRAW | CS_VREDRAW; // Redraw if the horizontal or vertical size changes		
	wcx.lpfnWndProc = WinProc; //pointer to the function that processes 
							   //the messages sent to the window
	wcx.cbClsExtra = 0; // how much extra memory the system should 
						//reserve for Window class. 0 = no extra class memory
	wcx.cbWndExtra = 0; // how much extra memory the system should reserve for
						//each window belonging to class. 0 = no extra window memory
	wcx.hInstance = hInstance; //Identifies the application that registered the class
							   //(Handle to instance)
	wcx.hIcon = NULL; //Defines the large icons used by Windows to represent the program
	wcx.hCursor = LoadCursor(NULL, IDC_ARROW); //mouse cursor used in the window. 
											   //here standard mouse pointer arrow.
											   // Background brush
	wcx.hbrBackground = (HBRUSH)GetStockObject(/*BLACK_BRUSH*/GRAY_BRUSH); //color and/or pattern 
																		   //used to fill the background of the window
	wcx.lpszMenuName = NULL; // Defines a default menu for windows that dont define own
							 //NUL = no defined menu
	wcx.lpszClassName = CLASS_NAME; //dentifies this class from other registered classes.
									//This class name must be used again in a call to the CreateWindow function
	wcx.hIconSm = NULL; // small icon used in the window�s title bar and on the start menu

						// Register the window class
						// RegisterClassEx returns 0 on error
	if (RegisterClassEx(&wcx) == 0) // If error
		return false;

	// Set up the screen in windowed or fullscreen mode?
	DWORD style;
	if (FULLSCREEN)
		style = WS_EX_TOPMOST | WS_VISIBLE | WS_POPUP;
	else
		style = WS_OVERLAPPEDWINDOW;

	//create and adjust window size
	RECT wr = { 0, 0, GAME_WIDTH, GAME_HEIGHT };    // set the size, but not the position
	// adjust the size
	if (!FULLSCREEN && !AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE))
	{
		MessageBox(NULL, "Error while adjusting window size", "ADJUST ERROR", MB_OK);
		return false;
	}

	// Create window
	hwnd = CreateWindow(
		CLASS_NAME, // Name of the window class
		GAME_TITLE, // Title bar text
		style, // style of window to create. 
			   //Here a resizable window with the familiar controls.
			   //For games: WS_OVERLAPPED
			   //(fixed size window with no controls)
		CW_USEDEFAULT, // Default horizontal position of window
		CW_USEDEFAULT, // Default vertical position of window
		wr.right - wr.left, // Width of window in pixels
		wr.bottom - wr.top, // Height of the window in pixels
		(HWND)NULL, // No parent window
		(HMENU)NULL, // No menu
		hInstance, // Handle to application instance
		(LPVOID)NULL); // No window parameters

					   // If there was an error creating the window
	if (!hwnd)
		return FALSE; // Cannot proceed so exit

	if (!FULLSCREEN) // If window
	{
		// Adjust window size so client area is GAME_WIDTH x GAME_HEIGHT
		RECT clientRect;
		GetClientRect(hwnd, &clientRect); // Get size of client area of window
		MoveWindow(hwnd,
			0, // Left
			0, // Top
			GAME_WIDTH + (GAME_WIDTH - clientRect.right), // Right
			GAME_HEIGHT + (GAME_HEIGHT - clientRect.bottom), // Bottom
			TRUE); // Repaint the window
	}

	// Show the window
	ShowWindow(hwnd, nCmdShow);
	// Send a WM_PAINT message to the window procedure
	UpdateWindow(hwnd);
	return true;
}

//=============================================================================
// clear window 
// Returns: false on error
//=============================================================================
void clearWindow(HWND &hwnd, HINSTANCE hInstance)
{
	// Show the mouse cursor.
	ShowCursor(true);
	// Fix the display settings if leaving full screen mode.
	if (FULLSCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}
	// Remove the window.
	DestroyWindow(hwnd);
	hwnd = NULL;
	// Remove the application instance.
	UnregisterClass(CLASS_NAME, hInstance);
}

//======================================================================
// Checks for another instance of the current application
// Returns: true if another instance is found
// false if this is the only one
//======================================================================
bool AnotherInstance()
{
	HANDLE ourMutex;

	//mutex is an object that may be owned by only one thread at a time

	// Attempt to create a mutex using our unique string
	ourMutex = CreateMutex(NULL, true,
		"Use_a_different_string_here_for_each_program_48161-XYZZY");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		return true; // Another instance was found
	return false; // We are the only instance
}


//======================================================================
// creates console for debug
//======================================================================
bool createConsole() 
{
	//Allocates a new console for the calling process.
	if (AllocConsole()==0)
	{
		return false;
	}
	//Reopen streams with different mode
	FILE *ptr; //useless, onlyfor calling freopen_s(first param)

	if (freopen_s(&ptr, "CONIN$", "r", stdin) !=0) 
	{
		return false;
	}
	if (freopen_s(&ptr, "CONOUT$", "w", stdout) != 0)
	{
		return false;
	}
	if (freopen_s(&ptr, "CONOUT$", "w", stderr) != 0)
	{
		return false;
	}
	return true;
}
