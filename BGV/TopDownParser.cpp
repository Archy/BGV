#include "TopDownParser.h"
#include "Constants.h"
#include "HyperbolicMatrix.h"

#include <algorithm>

TopDownParser::TopDownParser()
{
}

TopDownParser::~TopDownParser()
{
}


void TopDownParser::parse(const std::shared_ptr<Node>& root)
{
	root->setPosition(0.0f, 0.0f, 0.0f, 0.0f);
	countAngles(root);
}


void TopDownParser::countAngles(const std::shared_ptr<Node>& node)
{
	sortChildren(node);
	placeKids(node);
	if (node->phi > 1.57)
	{
		node->radius = std::asinh(std::sqrt((std::pow(sinh(node->radius), 2)*(1 - std::cos(node->phi)))*1.076));
		placeKids(node);
	}
}


void TopDownParser::placeKids(const std::shared_ptr<Node>& node)
{
	double maxRadius = 0.0;
	double dTheta = 0.0, dPhi = 0.0;
	bool newLayer = false;

	const double radius = node->radius;
	double phi = 0.0;
	double theta = 0.0;
	for (auto child : node->children)
	{
		if (phi != 0.0 || theta != 0.0 || newLayer)
		{
			if (phi != 0.0)
			{
				dTheta = std::atan(std::tanh(child->radius)/(std::sin(phi)*std::sinh(radius)));
				if (theta + dTheta > 2*PI)
				{
					theta -= 2 * PI;
					newLayer = true;
				}
			}
			if (newLayer)
			{
				phi += std::atan(std::tanh(maxRadius)/std::sinh(radius));
				dTheta = std::atan(std::tanh(child->radius) / (std::sin(phi)*std::sinh(radius)));
				newLayer = false;
				maxRadius = child->radius;
			}
		}
		theta += dTheta;
		
		if (child->radius > maxRadius)
		{
			maxRadius = child->radius;
		}

		if (phi == 0.0) {
			newLayer = true;
		}
		else 
		{
			dTheta = std::atan(std::tanh(child->radius) / (std::sin(phi)*std::sinh(radius)));
			theta += dTheta;
		}

		child->phi = phi;
		child->theta = theta;
	}

	for (auto child : node->children)
	{
		countAngles(child);
	}
}

void TopDownParser::setPositions(const std::shared_ptr<Node>& node)
{
}

void TopDownParser::sortChildren(const std::shared_ptr<Node>& node)
{
	if (!node->isLeaf())
	{
		auto children = node->getChildren();
		std::sort(children.begin(), children.end(), 
			[](std::shared_ptr<Node> a, std::shared_ptr<Node> b) -> bool
			{	
				return a->getRadius() > b->getRadius();
			}
		);
	}
}

double TopDownParser::computeDeltaPhi(const double nodeRadius, const double parentRadius)
{
	return std::atan(std::tanh(nodeRadius) / std::sinh(parentRadius));
}

double TopDownParser::computeDeltaTheta(const double nodeRadius, const double parentRadius, double phi)
{
	return std::atan(std::tanh(nodeRadius) / (std::sinh(parentRadius) * std::sin(phi)));
}
