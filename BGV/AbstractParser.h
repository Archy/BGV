#pragma once
#include "Node.h"

class AbstractParser
{
public:
	virtual void parse(const std::shared_ptr<Node>& root) = 0;
};

