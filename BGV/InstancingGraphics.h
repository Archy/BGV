#pragma once
#include "Graphics.h"
#include "InstancingModel.h"
#include "InstancingShader.h"
#include "InstancingLine.h"
#include <iostream>
#include <DirectXCollision.h>
#include <limits>
#include "Graph.h"

class Graph;

class InstancingGraphics :
	public Graphics
{
public:
	InstancingGraphics();
	~InstancingGraphics();

	virtual void Init(const HWND hWnd);

	//TODO set as private -> useless now
	void prepareInstancing(const std::string &modelName);

	//TOOD delete
	void RenderCurrentInstance(const DirectX::XMMATRIX& worldMatrix,
		int indexOffset = 0, int vertexOffset = 0, UINT indexCount = 0);
	void RenderCurrentInstance(int instanceCount, int instanceOffset = 0, 
		int indexOffset = 0, int vertexOffset = 0, UINT indexCount = 0);
	void RenderCurrentInstanceLine(int instanceCount, int instanceOffset = 0, int vertexOffset = 0);

	std::shared_ptr<Model> addInstancingModel(const std::string &name, const std::vector<Model::Vertex> &vertices,
		const std::vector<UINT> &indices);
	std::shared_ptr<Model> addInstancinLine(const std::string &name, const std::vector<Model::Vertex> &vertices);

	void createInstancedBuffer(const std::vector<Model::InstancedData> 
		&instancedData, std::string modelName);


	//color changing test
	void testColourChange(const std::string &modelName, const int index, const DirectX::XMFLOAT4 colour)
	{
		std::shared_ptr<InstancingModel> iModel;

		if (findInstancingModel(modelName, iModel))
		{
			iModel->changeColour(mD3D->GetDeviceContext(), index, colour);
		}
		else
		{
			std::cout << "Model: " << modelName << " is not an instancing model" << std::endl;
			//TODO throw exception
		}
	}

	void pickNode(const int mouseX, const int mouseY, std::unique_ptr<Graph> &graph)
	{
		if (graph->getNodes().size() == 0)
			return;

		static int active_index = -1;
		DirectX::XMMATRIX P;
		mCamera->GetProjectionMatrix(P);

		// Oblicz promie� wskazuj�cy w przestrzeni widoku.
		float vx = (+2.0*mouseX / mClientWidth - 1.0) / (double)DirectX::XMVectorGetX(P.r[0]);	//P(0, 0);
		float vy = (-2.0*mouseY / mClientHeight + 1.0) / (double)DirectX::XMVectorGetY(P.r[1]);	//P(1, 1);
		
		// Definicja promienia w przestrzeni widoku.
		const DirectX::XMVECTOR rayOrigin = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
		const DirectX::XMVECTOR rayDir = DirectX::XMVectorSet(vx, vy, 1.0f, 0.0f);

		//bounding box of side equal rendered node side and placed at (0,0,0)
		float size = static_cast<float>(graph->getCubeWidth());
		DirectX::BoundingBox bbox(DirectX::XMFLOAT3(0.0, 0.0, 0.0), DirectX::XMFLOAT3(size, size, size));
		auto nodes = graph->getNodes();
		float currentDist = (std::numeric_limits<float>::max)();
		std::shared_ptr<Node> currentNode;


		DirectX::XMMATRIX temp;
		mCamera->GetViewMatrix(temp);
		DirectX::XMMATRIX V = temp;
		const DirectX::XMMATRIX invView = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(V), V);

		int count = 0;
		int currentActive = -1;
		for (auto &n : nodes)
		{
			++count;

			//cast ray to nodes local space
			DirectX::XMMATRIX W = n->getWorldMatrix(); //DirectX::XMLoadFloat4x4(&mMeshWorld);
			DirectX::XMMATRIX invWorld = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(W), W);
			
			DirectX::XMMATRIX toLocal = DirectX::XMMatrixMultiply(invView, invWorld);
			
			auto localRayOrigin = DirectX::XMVector3TransformCoord(rayOrigin, toLocal);
			auto localRayDir = DirectX::XMVector3TransformNormal(rayDir, toLocal);
			
			// Nadaj kierunkowi promienia d�ugo�� 1 przed testami przeci�cia.
			localRayDir = DirectX::XMVector3Normalize(localRayDir);

			// origin, direction, dist (FXMVECOR, FXMVECTOR, float)
			float dist = (std::numeric_limits<float>::max)(); //intersect counts distance from origin to box !!!
			if (bbox.Intersects(localRayOrigin, localRayDir, dist))
			{
				std::cout << "\t\t" << dist << "\n";
				if (dist < currentDist)
				{
					currentActive = n->getMeshIndex(); //count;
					currentNode = n;
				}
			}
		}
		if(currentNode.operator bool())
			std::cout << currentNode->getID() << std::flush;
		else
			std::cout << "None" << std::endl;
		std::cout << "\t" << count << "\n";

		if (currentActive != -1)
		{
			//new highlighted node
			if(active_index != -1)
				testColourChange("cube", active_index, Colors::White);

			active_index = currentActive;
			std::cout << "\t\t\t\t" << currentActive << "\n";
			testColourChange("cube", active_index, Colors::Red);
		}
		else if (active_index != -1)
		{
			//no currently active node
			testColourChange("cube", active_index, Colors::White);
			active_index = -1;
		}
	}

private:
	bool findInstancingModel(const std::string &name, std::shared_ptr<InstancingModel> &model);
	bool findInstancingLine(const std::string &name, std::shared_ptr<InstancingLine> &model);


private:
	std::string mCurrentModelName;

	std::unique_ptr <InstancingShader> mInstancingShader;
};

