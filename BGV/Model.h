#pragma once
#include <d3d11.h>
#include <directxmath.h>
#include <vector>
#include <atlbase.h>
#include "Constants.h"
#include "GameError.h"

class Model
{
public:
	struct Vertex
	{
		DirectX::XMFLOAT3 Pos;
		DirectX::XMFLOAT4 Color;
	};
		struct InstancedData
	{
	public:
		InstancedData(DirectX::XMMATRIX w, DirectX::XMFLOAT4 Color)
		{
			DirectX::XMStoreFloat4x4(&World, w);
			Color2 = Color;
		}

		DirectX::XMFLOAT4X4 World;
		DirectX::XMFLOAT4 Color2;
	};

public:
	Model();
	~Model();

	virtual void Initialize(CComPtr<ID3D11Device> dev, 
		const std::vector<Vertex> &vertices, const std::vector<UINT> &indices);

	virtual void Render(CComPtr<ID3D11DeviceContext> &devCon);

	int GetIndexCount() const { return mIndexCount; }

protected:
	CComPtr<ID3D11Buffer> mVB;
	CComPtr<ID3D11Buffer> mIB;
	int mVertexCount, mIndexCount;
};

