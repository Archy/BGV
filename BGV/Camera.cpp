#include "Camera.h"



Camera::Camera() :
	mPositionX(4.0), mPositionY(4.0), mPositionZ(5.0),
	SCREEN_DEPTH(1000.0), SCREEN_NEAR(1.0)
{
}


Camera::~Camera()
{
}

void * Camera::operator new(size_t i)
{
	return _mm_malloc(i,16);
}

void Camera::operator delete(void * p)
{
	_mm_free(p);
}

void Camera::SetPosition(float x, float y, float z)
{
	mPositionX = x;
	mPositionY = y;
	mPositionZ = z;
}

void Camera::SetRotation(float x, float y, float z)
{
	mRotationX = x;
	mRotationY = y;
	mRotationZ = z;
}

void Camera::setLens(float width, float height)
{
	float fieldOfView = 3.141592654f / 4.0f;
	float screenAspect = width / height;

	// Create the projection matrix for 3D rendering.
	mProjectionMatrix = DirectX::XMMatrixPerspectiveFovLH(
		fieldOfView,	// PL: pionowy k�t pola widzenia w radianach
		screenAspect,	// PL: proporcje obrazu = szeroko��/wysoko��
		SCREEN_NEAR,		// PL: odleg�o�� bli�szej p�aszczyzny
		SCREEN_DEPTH);	// PL: odleg�o�� dalszej p�aszczyzny
}

void Camera::Render()
{
	// Build the view matrix.
	DirectX::XMVECTOR pos = DirectX::XMVectorSet(mPositionX, mPositionY, mPositionZ, 1);
	DirectX::XMVECTOR target = DirectX::XMVectorZero();
	DirectX::XMVECTOR up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	mViewMatrix = DirectX::XMMatrixLookAtLH(pos, target, up);
}
