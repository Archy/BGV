#include "HypGraphRenderer.h"
#include "HyperbolicMatrix.h"
#include <iostream>

using namespace DirectX;


HypGraphRenderer::HypGraphRenderer()
{
}


HypGraphRenderer::~HypGraphRenderer()
{
}

void HypGraphRenderer::parse(const std::shared_ptr<Node>& root)
{
	root->setPosition(0.0f, 0.0f, 0.0f, 1.0f);
	renderNode(root, DirectX::XMVectorSet(0, 0, -1, 0));
}

void HypGraphRenderer::renderNode(const std::shared_ptr<Node>& node, DirectX::XMVECTOR initialDir)
{
	if (node->childrenSize() == 0)
		return;

	DirectX::XMVECTOR cross = DirectX::XMVector3Cross(initialDir,
		DirectX::XMVectorSet(1, 1, 1, 0));
	cross = DirectX::XMVectorSetW(cross, DirectX::XMVectorGetW(cross));

	DirectX::XMVECTOR parentPos = DirectX::XMVectorSet(node->getXFloat(), node->getYFloat(), node->getZFloat(), node->getWFloat());

	HypMat::normalize(cross);
	HypMat::normalize(initialDir);

	const double radius = node->getRadius();
	const double sinhRadius = std::sinh(radius);

	//set children positions
	for (auto child : node->getChildren())
	{
		const double phi = child->getPhi();
		const double theta = child->getTheta();

		if (phi == 0.0)
		{
			auto deltaDir = DirectX::XMVectorScale(initialDir, radius);
			auto position = HypMat::add(parentPos, deltaDir);

			DirectX::XMFLOAT4 tempPos;
			DirectX::XMStoreFloat4(&tempPos, position);
			child->setPosition(tempPos.x, tempPos.y, tempPos.z, tempPos.w);
			std::cout << tempPos.x << " " << tempPos.y << " " << tempPos.z << "\n";

			renderNode(child, HypMat::sub(parentPos, position));
		}
		else
		{
			const double B = std::asinh(sinhRadius * std::sin(phi));
			const double A = std::asinh(std::tanh(B) / std::tan(phi));

			auto deltaDir = DirectX::XMVectorScale(initialDir, A);
			auto deltaCross = DirectX::XMVectorScale(cross, B);

			auto dirPos = HypMat::add(parentPos, deltaDir);
			//auto crossPos = HypMat::add(dirPos, deltaCross);
			//auto rotationMatrix = HypMat::rotationMatrix(parentPos, dirPos, theta);
			//auto position = DirectX::XMVector4Transform(crossPos, rotationMatrix);

			auto rotationMatrix = DirectX::XMMatrixRotationAxis(dirPos - parentPos, theta);
			auto crossPos = DirectX::XMVectorScale(DirectX::XMVector4Transform(cross, rotationMatrix), B);
			auto position = dirPos + crossPos;


			
			DirectX::XMFLOAT4 tempPos;
			DirectX::XMStoreFloat4(&tempPos, position);
			child->setPosition(tempPos.x, tempPos.y, tempPos.z, tempPos.w);
			
			renderNode(child, HypMat::sub(parentPos, position));
		}
	}
}
