#pragma once
#include <atlbase.h>
//include the Direct3D 11 library files
//first param(lib)->indicates that we want to add a library file to the project
//second param->specifies file
#pragma comment(lib, "d3d11.lib")	//functionality for setting up and drawing
#pragma comment(lib, "dxgi.lib")	//tools to interface with the hardware on the computer
#pragma comment(lib, "d3dcompiler.lib") //functionality for compiling shaders

#include <d3d11.h>	//core pieces of Direct3D
#include <directxmath.h>

__declspec(align(16)) class D3DClass
{
public:
	D3DClass();
	~D3DClass();

	void* operator new(size_t i);
	void operator delete(void* p);


	void InitD3D(HWND hWnd, const int width, const int height);     // initialize D3D, throws GameError on failure 
	void Resize(const int width, const int height);

	void BeginScene();
	void EndScene();

	CComPtr<ID3D11Device> GetDevice() { return mDev; }
	CComPtr<ID3D11DeviceContext> GetDeviceContext() { return mDevCon; }

	//void GetProjectionMatrix(DirectX::XMMATRIX& projectionMatrix);
	void GetWorldMatrix(DirectX::XMMATRIX& worldMatrix);
	void GetOrthoMatrix(DirectX::XMMATRIX& orthoMatrix);

	void setRasterizerStateSolid();
	void setRasterizerStateWireframe();

private:
	void createDeviceAndContext(const HWND hWnd);
	void createSwapchain(const HWND hWnd, const int width, const int height);
	void createDepthStencilBuffer(const int width, const int height);
	void setViewpoint(const int width, const int height);
	void setRenderTarget();
	void setMatrices(const int width, const int height);
	
	void createRasterizerStateSolid();
	void createRasterizerStateWireframe();

private:
	// The pointer to D3D device interface (COM object). 
	// Used to allocate resources and check feature support
	CComPtr<ID3D11Device> mDev;
	// The pointer to D3D device context (COM object).  
	// Used to set render states, bind resources to 
	// graphic pipeline and issue rendering commands
	CComPtr<ID3D11DeviceContext> mDevCon;           
	// the pointer to the swap chain interface (COM object)
	CComPtr<IDXGISwapChain> mSwapchain;             
	//backbuffer texture for rendering
	CComPtr<ID3D11RenderTargetView> mBackBufferView;
	
	CComPtr<ID3D11Texture2D> mDepthStencilBuffer;
	CComPtr<ID3D11DepthStencilView> mDepthStencilView;

	CComPtr<ID3D11RasterizerState> mRasterStateSolid;
	CComPtr<ID3D11RasterizerState> mRasterStateWireframe;

	DirectX::XMMATRIX mWorldMatrix;
	DirectX::XMMATRIX mOrthoMatrix;

	D3D_FEATURE_LEVEL mFeatureLevel;			// flag of the highest feature level that was found

	bool mEnable4xMsaa;
	bool mVsync;
};

