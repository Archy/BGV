#pragma once
#include "AbstractParser.h"

class HypGraphRenderer : public AbstractParser
{
public:
	HypGraphRenderer();
	~HypGraphRenderer();

	virtual void parse(const std::shared_ptr<Node>& root) override;

private:
	void renderNode(const std::shared_ptr<Node>& node, DirectX::XMVECTOR initialDir);
};

