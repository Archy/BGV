#pragma once
#include "Model.h"
class InstancingModel :
	public Model
{
public:
	InstancingModel();
	~InstancingModel();

	virtual void Render(CComPtr<ID3D11DeviceContext> &devCon);

	void createInstancedBuffer(CComPtr<ID3D11Device> &devCon,
		const std::vector<Model::InstancedData> &instancedData);

	void changeColour(CComPtr<ID3D11DeviceContext> &mDevCon,
						const int index, const DirectX::XMFLOAT4);

protected:
	CComPtr<ID3D11Buffer> mInstancedBuffer;
};

