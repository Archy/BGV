#include "MovingCamera.h"



MovingCamera::MovingCamera()
{
	updatePosition();
}


MovingCamera::~MovingCamera()
{
}

void MovingCamera::rotate(float dTheta, float dPhi)
{
	// Update angles based on input to orbit camera around box.
	mTheta += dTheta;
	mPhi += dPhi;

	// Restrict the angle mPhi.
	if (mPhi < 0.1f)
		mPhi = 0.1f;
	if (mPhi > 3.1415f)
		mPhi = 3.1415f;

	updatePosition();
}

void MovingCamera::move(float dRadius)
{
	mRadius -= dRadius;
	// Restrict the radius.
	if (mRadius < MIN_RADIUS)
		mRadius = MIN_RADIUS;
	if (mRadius > MAX_RADIUS)
		mRadius = MAX_RADIUS;

	updatePosition();
}

void MovingCamera::updatePosition()
{
	float x = mRadius*sinf(mPhi)*cosf(mTheta);
	float z = mRadius*sinf(mPhi)*sinf(mTheta);
	float y = mRadius*cosf(mPhi);
	SetPosition(x, y, z);
}
