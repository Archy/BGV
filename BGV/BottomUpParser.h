#pragma once
#include "Node.h"
#include "AbstractParser.h"

class BottomUpParser : public AbstractParser
{
private:
	static const double LEAF_RADIUS;
	static const double MAX_RADIUS;

public:
	BottomUpParser();
	~BottomUpParser();

	virtual void parse(const std::shared_ptr<Node>& root) override;

private:
	double countRadius(const std::shared_ptr<Node>& node);
};

