#pragma once
#include <string>
#include <Windows.h>

class InputHandler
{
public:
	InputHandler();
	~InputHandler();

	void Initialize(HWND hwnd);
//mouse input
	void mouseLDOWN() { mMouseLeft = true; }
	void mouseLUP() { mMouseLeft = false; }

	void mouseRDOWN() { mMouseRight = true; };
	void mouseRUP() { mMouseRight = false; };

	bool isMouseL() { return mMouseLeft; }
	bool isMouseR() { return mMouseRight; }


	void setMousePos(int x, int y);

	int getMouseX() { return mMouseX; }
	int getMouseY() { return mMouseY; }


	void mouseWheelMove(WPARAM wParam);
	int getMouseWheelMove() { return mMouseWheel; }
	bool wasMouseWheelMoved() { return mMouseWheelMoved; }


//keyboard input
	void keyDown(WPARAM wParam);
	void keyUp(WPARAM wParam);
	void clearKeysPress();

//keyborad getter funcions
	bool isKeyDown(UCHAR vkey) const;
	bool wasKeyPressed(UCHAR vkey) const;
	bool anyKeyPressed() const;

	enum KEYS {
		RIGHT_ARROW = VK_RIGHT,
		LEFT_ARROW = VK_LEFT,
		UP_ARROW = VK_UP,
		DOWN_ARROW = VK_DOWN,
		ESC_KEY = VK_ESCAPE,
		ENTER_KEY = VK_RETURN
	};

private:
//mouse
	bool mMouseLeft;
	bool mMouseRight;
	int mMouseX;
	int mMouseY;

	int  mMouseWheel;
	bool mMouseWheelMoved;

//keyboard
	constexpr static UINT KEYS_ARRAY_LEN = 256;
	bool mKeysDown[KEYS_ARRAY_LEN];     // true if specified key is down
	bool mKeysPressed[KEYS_ARRAY_LEN];  // true if specified key was pressed

};