#pragma once
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <atlbase.h>
#include "d3dx11Effect.h"

#include "Constants.h"
#include "GameError.h"

class BaseShader
{
public:
	BaseShader();
	~BaseShader();

	//initialize shaders. Throws exception on error
	void Initialize(CComPtr<ID3D11Device> device, const std::string &shaderFile);

protected:
	virtual void createLayout(CComPtr<ID3D11Device> device) = 0;

protected:
	CComPtr<ID3DX11Effect> mFX;
	CComPtr<ID3DX11EffectTechnique> mTech;
	CComPtr<ID3DX11EffectMatrixVariable> mfxWorldViewProj;

	CComPtr<ID3D11InputLayout> mLayout;

private:
	void InitializeShader(CComPtr<ID3D11Device> device, const std::string &shaderFile);
};

