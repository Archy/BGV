#include <iostream>
#include <fstream>
#include <DirectXMath.h>
#include "Box.h"

Box::Box()
{
}

Box::~Box()
{
}

void Box::Initialize(HWND hwnd)
{
	Game::Initialize(hwnd);
	createBox();
	createLine();

	mGraph = std::make_unique<Graph>();
}

void Box::Update()
{
	if (mInput->wasKeyPressed('l') || mInput->wasKeyPressed('L'))
	{
		//TODO chosing file
		std::cout << "Loading graph!" << std::endl;
		mGraph->loadGraph("test.graphml");

		mGraphic->createInstancedBuffer(mGraph->getInstancedData(), "cube");
		mGraphic->createInstancedBuffer(mGraph->getIndicesInstancedData(), "line");
	}

	if (mInput->isMouseL())
	{
		// Make each pixel correspond to a quarter of a degree.
		float dx = DirectX::XMConvertToRadians(0.25f*static_cast<float>(mMouse.x - mInput->getMouseX()));
		float dy = DirectX::XMConvertToRadians(0.25f*static_cast<float>(mMouse.y - mInput->getMouseY()));

		mGraphic->rotateCamera(dx, dy);
	}
	else if (mInput->wasMouseWheelMoved())
	{
		// Update the camera radius based on input.
		// Make each roll correspond to 0.005 unit in the scene.
		mGraphic->moveCamera(0.015f*mInput->getMouseWheelMove());
	}
	//highlight node pointed by mouse cursor
	//probably works only for nodes within some distance from camera
	mGraphic->pickNode(mInput->getMouseX(), mInput->getMouseY(), mGraph);

	mMouse.x = mInput->getMouseX();
	mMouse.y = mInput->getMouseY();
}

void Box::Render()
{
	mGraphic->beginScene();

	//if graph has been loaded
	if (mGraph->operator bool())
	{
		//render nodes
		mGraphic->prepareInstancing("cube");
		mGraphic->RenderCurrentInstance(static_cast<int>(mGraph->getChildrenNr()));

		//render indices
		mGraphic->setRasterizerStateWireframe(); //D3D11_PRIMITIVE_TOPOLOGY_LINELIST 
												//are consider in depth buffer only
												// when rendering in wireframe mode
		mGraphic->prepareInstancing("line");
		mGraphic->RenderCurrentInstanceLine(mGraph->getChildrenNr() - 1);
	}


	mGraphic->endScene();
}

void Box::createBox()
{
	auto color = Colors::LightSalmon;
	float size = static_cast<float>(Graph::getCubeWidth());
	//create verices
	std::vector<Model::Vertex> vertices =
	{
		{ DirectX::XMFLOAT3(-size, -size, -size), color },
		{ DirectX::XMFLOAT3(-size, +size, -size), color },
		{ DirectX::XMFLOAT3(+size, +size, -size), color },
		{ DirectX::XMFLOAT3(+size, -size, -size), color },
		{ DirectX::XMFLOAT3(-size, -size, +size), color },
		{ DirectX::XMFLOAT3(-size, +size, +size), color },
		{ DirectX::XMFLOAT3(+size, +size, +size), color },
		{ DirectX::XMFLOAT3(+size, -size, +size), color }
	};

	std::vector<UINT> indices = {
		// front face
		0, 1, 2,
		0, 2, 3,
		// back face
		4, 6, 5,
		4, 7, 6,
		// left face
		4, 5, 1,
		4, 1, 0,
		// right face
		3, 2, 6,
		3, 6, 7,
		// top face
		1, 5, 6,
		1, 6, 2,
		// bottom face
		4, 0, 3,
		4, 3, 7
	};

	mGraphic->addInstancingModel("cube", vertices, indices);
}

void Box::createLine()
{
	auto color = Colors::Green;
	float lenght = 1;
	
	//create line for D3D11_PRIMITIVE_TOPOLOGY_LINELIST 
	//create verices
	std::vector<Model::Vertex> vertices =
	{
		{ DirectX::XMFLOAT3(0, 0, 0), color },
		{ DirectX::XMFLOAT3(lenght, 0, 0), color }
	};
	mGraphic->addInstancinLine("line", vertices);
}
