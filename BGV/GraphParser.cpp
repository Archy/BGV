#include "GraphParser.h"
#include <exception>
#include <Windows.h>
#include <iostream>

GraphParser::GraphParser()
{
}


GraphParser::~GraphParser()
{
}

bool GraphParser::loadGraph(std::string fileName, 
	std::shared_ptr<Node>& root, std::vector<std::shared_ptr<Node>>& nodesVect)
{
	try 
	{
		//load .graphml file
		rapidxml::xml_document<> doc;
		rapidxml::file<> xmlFile = rapidxml::file<>(fileName.c_str());
		doc.parse<0>(xmlFile.data());

		//find root graph node of xmlfile (first node is graphml, we want graph)
		mXmlRoot = doc.first_node()->first_node("graph");
		if (mXmlRoot->first_attribute("root"))
			mRootId = mXmlRoot->first_attribute("root")->value();
		if (mRootId.empty())
			throw std::runtime_error("Root node not defined");

		//check if nodes and edges exist
		if(!mXmlRoot->first_node("node") || !mXmlRoot->first_node("edge"))
			throw std::runtime_error("Node/edge xml_node does not exists");

		ParseNodes(root, nodesVect);
		ParseEdges(nodesVect);
		//finish and clear
		doc.clear();
	}
	catch (std::exception &err)
	{
		MessageBox(NULL, err.what(), "Graph loading error", MB_OK);
		return false;
	}
	catch (...)
	{
		MessageBox(NULL, "Unkown error", "Graph loading error", MB_OK);
		return false;
	}

	return true;
}

void GraphParser::ParseNodes(std::shared_ptr<Node>& root, 
	std::vector<std::shared_ptr<Node>>& nodesVect)
{
	mNodesNr = 0;
	//parse and remember all nodes
	for (decltype(mXmlRoot->first_node()) nodeNode = mXmlRoot->first_node("node");
		nodeNode; nodeNode = nodeNode->next_sibling("node"))
	{
		std::string id = nodeNode->first_attribute("id")->value();
		nodesVect.push_back(std::make_shared<Node>(Node(id)));
		mNodes[id] = nodesVect.back();

		if (id == mRootId)
			root = mNodes[id];

		++mNodesNr;
	}

	std::cout << "Nodes nr: " << mNodesNr << std::endl;
}

void GraphParser::ParseEdges(std::vector<std::shared_ptr<Node>>& nodesVect)
{
	mEdgesNr = 0;
	//remember all indices
	for (decltype(mXmlRoot->first_node()) nodeNode = mXmlRoot->first_node("edge");
		nodeNode; nodeNode = nodeNode->next_sibling("edge"))
	{
		std::string source = nodeNode->first_attribute("source")->value();
		std::string target = nodeNode->first_attribute("target")->value();
		//set parents out edge and child in edge
		mNodes[source]->getChildren().push_back(mNodes[target]);
		mNodes[target]->setParent(mNodes[source]);

		++mEdgesNr;
	}

	std::cout << "Edges nr: " << mEdgesNr << std::endl;
}
