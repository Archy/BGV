#include "Game.h"
#include <sstream>
#include <iostream>
#include <WindowsX.h>

#define _USE_MATH_DEFINES

Game::Game()
{
	mInput = std::make_unique<InputHandler>();
	mTimer = std::make_unique<GameTimer>();
}

Game::~Game()
{
}

LRESULT Game::MessageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		// Tell Windows to kill this program
		PostQuitMessage(0);
		return 0;

	case WM_ACTIVATE:
		// WM_ACTIVATE is sent when the window is activated or deactivated.  
		// We pause the game when the window is deactivated and unpause it 
		// when it becomes active.  
		if (LOWORD(wParam) == WA_INACTIVE)
		{
			mPaused = true;
			mTimer->Stop();
		}
		else
		{
			mPaused = false;
			mTimer->Start();
		}
		return 0;

	case WM_SIZE:
		//user resize window
		if (mGraphic)
		{
			mGraphic->setClientSize(LOWORD(lParam), HIWORD(lParam));

			if (wParam == SIZE_MINIMIZED)
			{
				mPaused = true;
				std::cout << "minimazed" << std::endl;
			}
			else if (wParam == SIZE_MAXIMIZED)
			{
				mPaused = false;
				mGraphic->resize();
				std::cout << "maximazed - resizing" << std::endl;
			}
			else if (wParam == SIZE_RESTORED)
			{
				mPaused = false;
				mGraphic->resize();

				if (!mResizing)
				{
					std::cout << "resizing" << std::endl;
					mGraphic->resize();
				}
			}
		}
		return 0;

	case WM_ENTERSIZEMOVE:
		//user grabs the resize bars.
		mPaused = true;
		mResizing = true;
		mTimer->Stop();
		return 0;

	case WM_EXITSIZEMOVE:
		// user releases the resize bars.
		mPaused = false;
		mResizing = false;
		mTimer->Start();
		mGraphic->resize();
		std::cout << "resizing" << std::endl;
		return 0;

	case WM_GETMINMAXINFO:
		//prevent from creating to small window
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = GAME_WIDTH;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = GAME_HEIGHT;
		return 0;

	case WM_KEYDOWN: case WM_SYSKEYDOWN:    
		// key down
		mInput->keyDown(wParam);
		return 0;

	case WM_KEYUP: case WM_SYSKEYUP:        
		// key up
		mInput->keyUp(wParam);
		return 0;

	case WM_LBUTTONDOWN:
		std::cout << "aaaa\n";
		mInput->mouseLDOWN();
		return 0;
	case WM_RBUTTONDOWN:
		mInput->mouseRDOWN();
		return 0;
	case WM_LBUTTONUP:
		mInput->mouseLUP();
		return 0;
	case WM_RBUTTONUP:
		mInput->mouseRUP();
		return 0;

	case WM_MOUSEMOVE:
		mInput->setMousePos(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		return 0;

	case WM_MOUSEWHEEL:                     // mouse wheel move
		mInput->mouseWheelMove(wParam);
		return 0;
	}

	// Handle any messages the switch statement didn't
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

void Game::Initialize(HWND hwnd)
{
	mHwnd = hwnd;

	mGraphic = std::make_unique<InstancingGraphics>();
	mGraphic->Init(hwnd);
	
	mTimer->Reset();

	mIsRunning = true;
	mPaused = false;
}

void Game::Frame()
{
	mTimer->Tick();

	if (!mPaused)
	{
		CalculateFrameStats();
		Update();
	}
	else
	{
		Sleep(100);
	}

	Render();

	mInput->clearKeysPress();
}

void Game::CalculateFrameStats()
{
	// Code computes the average frames per second, and also the
	// average time it takes to render one frame. These stats
	// are appeneded to the window caption bar.
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;
	
	frameCnt++;
	
	// Compute averages over one second period.
	if ((mTimer->TotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;
		std::ostringstream outs;
		std::string title;

		outs.precision(6);
		outs << GAME_TITLE << " "
			<< "FPS: "<< fps << " "
			<< "Frame time: " << mspf << " (ms)";
		title = outs.str();
		SetWindowText(mHwnd, title.c_str());
		
		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}
