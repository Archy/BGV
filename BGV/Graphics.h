#pragma once
#include <memory>
#include <map>
#include <string>
#include "D3D.h"
#include "ColorShader.h"
#include "Model.h"
#include "MovingCamera.h"


//class for handlig DX11 graphics components
__declspec(align(16)) class Graphics
{
public:
	Graphics();
	~Graphics();
	void* operator new(size_t i);
	void operator delete(void* p);

	void beginScene();
	void endScene();
	void Render(const std::string &modelName, const DirectX::XMMATRIX& worldMatrix,
		int indexOffset=0, int vertexOffset=0, UINT indexCount=0);
	void Render(const std::string &modelName, int indexOffset = 0,
		int vertexOffset = 0, UINT indexCount = 0);

	virtual void Init(const HWND hWnd);     // initialize D3D, throws GameError on failure 

	void rotateCamera(const float dTheta, const float dPhi);
	void moveCamera(const float dRadius);

	std::shared_ptr<Model> addModel(const std::string &name, const std::vector<Model::Vertex> &vertices,
		const std::vector<UINT> &indices);

	void setClientSize(const int width, const int height);
	void resize();
	float aspectRatio() { return static_cast<float>(mClientHeight) / mClientHeight; }

	void setRasterizerStateSolid();
	void setRasterizerStateWireframe();

protected:
	int mClientWidth = GAME_WIDTH;
	int mClientHeight = GAME_HEIGHT;

	std::unique_ptr <D3DClass> mD3D;
	std::unique_ptr <ColorShader> mFX;
	std::map<std::string, std::shared_ptr <Model>> mModels;
	std::unique_ptr <MovingCamera> mCamera;

	DirectX::XMMATRIX mViewProjectionMatrix;
	DirectX::XMMATRIX mViewMatrix;
	DirectX::XMMATRIX mProjectionMatrix;
};
