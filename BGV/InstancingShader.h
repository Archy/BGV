#pragma once
#include "BaseShader.h"

class InstancingShader :
	public BaseShader
{
public:
	InstancingShader();
	~InstancingShader();

	//sets the shader parameters and then draws the prepared model vertices.
	//Throws expetion on error
	void Render(CComPtr<ID3D11DeviceContext> deviceContext,
		int indexCount, int InstanceCount, int instanceOffset,
		int indexOffset, int vertexOffset,
		const DirectX::XMMATRIX &viewProjectionMatrix);

	void RenderLine(CComPtr<ID3D11DeviceContext> deviceContext,
		int InstanceCount, int instanceOffset, int vertexOffset,
		const DirectX::XMMATRIX &viewProjectionMatrix);

private:
	virtual void createLayout(CComPtr<ID3D11Device> device);

	bool SetShaderParameters(CComPtr<ID3D11DeviceContext> deviceContext,
		const DirectX::XMMATRIX &viewProjectionMatrix);

	void RenderShader(CComPtr<ID3D11DeviceContext> deviceContext,
		int indexCount, int InstanceCount, int instanceOffset,
		int indexOffset, int vertexOffset);
	void RenderShaderNonIndexed(CComPtr<ID3D11DeviceContext> deviceContext,
		int InstanceCount, int instanceOffset, int vertexOffset);
};

